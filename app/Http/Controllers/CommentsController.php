<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\HikakinJunken;
use App\Models\Comment;

class CommentsController extends Controller
{
    //
    public function store(Request $request, HikakinJunken $hikakin_junken) {
        $this->validate($request, [
          'body' => 'required'
        ]);
        // $md = new HikakinJunken();
        // $md->Comments($request->body, $videoid);
        $comment = new Comment(['body' => $request->body]);
        $hikakin_junken->comments()->save($comment);
        return redirect()->action('VideoViewController@watch', $hikakin_junken);
      }
}
