<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Mail;
use App\Mail\ContactNotification;

class ContactController extends Controller
{

    private $email_str = "/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/";

    /* メールアドレス判定 */
    private function is_email($email) {
        if(preg_match($this->email_str, $email)) {
            // echo "email: OK <br>";
            return TRUE;
        } else {
            // echo "email: False <br>";
            return FALSE;
        }
    }

    /* 名前の文字数カウント */
    private function name_count($text) {
        if(mb_strlen($text) > 51 || mb_strlen($text) <= 0) {
            return FALSE;
        } else {
            // echo "名前：" . mb_strlen($text) . "<br>";
            return TRUE;
        }
    }

    /* お問い合わせ文の文字数カウント */
    private function contacttext_count($text) {
        if(mb_strlen($text) > 1001 || mb_strlen($text) <= 0) {
            return FALSE;
        } else {
            // echo "お問い合わせ：" . mb_strlen($text) . "<br>";
            return TRUE;
        }
    }

    // お問い合わせページにアクセス
    public function contact(Request $request) {

        $request->session()->regenerateToken();
        $no_error_massage = FALSE;
        $mail_send_message = "";

        // 名前を受け取る
        $name = $request->input('name');
        // メールアドレスを受け取る
        $email = $request->input('mail');
        // お問い合わせ内容を受け取る
        $contact_text = $request->input('contact');

        // 指定した文字数カウントかどうか判定
        $name_flag = $this->name_count($name);
        $contacttext_flag = $this->contacttext_count($contact_text);
        //　メールアドレスかどうか判定
        $email_flag = $this->is_email($email);

        if($name == "" && $email == "" && $contact_text == "") {
            $name_flag = FALSE;
            $contacttext_flag = FALSE;
            $email_flag = FALSE;
            $no_error_massage = TRUE;
        }

        // 指定した文字数カウントとメールアドレスが正しいか判定し、メールを送信
        if($name_flag == TRUE && $contacttext_flag == TRUE && $email_flag == TRUE){
            
            $to = 'dhiki@hikakinjunken.ml';

            Mail::to($to)->send(new ContactNotification($name, $contact_text, $email));

            // エラーメッセージを空にする
            $name_error = "";
            $contacttext_error = "";
            $email_error = "";
            $no_error_massage = TRUE;

            // メール送信成功のメッセージを表示する
            $mail_send_message = "お問い合わせ内容の送信が完了しました。<br>
                                内容によって返事が難しい場合もございます。<br>
                                ご了承ください。<br>
                                ※以下のメールアドレス宛に返事をお返しする予定です。<br>
                                ※メールアドレスを間違えますと受信することができませんので、間違いの場合は再度送信をお願いします。<br>
                                <span class='responce_mail'>" . $email . "</span>";
        } 
        
        // エラーメッセージと文字数を各項目の制限までに調整する
        if($name_flag == FALSE && $no_error_massage != TRUE) {
            $name_error = "※名前が50文字超えているか、または入力されていません。";
            $name = mb_substr($name, 0, 50);
        } else {
            $name_error = "";
        }
        if($email_flag == FALSE && $no_error_massage != TRUE) {
            $email_error = "※メールアドレスの形式ではありません。または入力されておりません。";
            $email = mb_substr($name, 0, 100);
        } else {
            $email_error = "";
        }
        if($contacttext_flag == FALSE && $no_error_massage != TRUE) {
            $contacttext_error = "※お問い合わせ文が1000文字超えているか、または入力されていません。";
            $contact_text = mb_substr($contact_text, 0, 1000);
        } else {
            $contacttext_error = "";
        }

        //　デフォルトのお問い合わせページレイアウト
        $contact_layout = "layouts.contactdefault";

        // ビューを返す
        return view('contact')->with([
            'contact_layout' => $contact_layout,
            'name_error' => $name_error,
            'contacttext_error' => $contacttext_error,
            'email_error' => $email_error,
            'mail_send_message' => $mail_send_message,
            'name' => $name, 
            'email' => $email, 
            'contact_text' => $contact_text
        ]);
    }
}
