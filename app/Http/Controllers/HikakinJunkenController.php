<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\HikakinJunken;

class HikakinJunkenController extends Controller{

    //
    public function model($type=null){
        // HikakinJunkenモデルのインスタンス化
        $md = new HikakinJunken();
        // データ取得
        $data = $md->SearchgetData($type);
    
        // ビューを返す
        return view('hikakinjunken.model', ['data' => $data]);
    }

    public function index(){

        // HikakinJunkenモデルのインスタンス化
        $md = new HikakinJunken();
        // 全体のジャンケン総数のデータ取得
        $total = $md->IndexgetData("total");
        $goo_total = $md->IndexgetData("goo_total");
        $par_total = $md->IndexgetData("par_total");
        $choki_total = $md->IndexgetData("choki_total");

        // 最新ジャンケンの日付
        $recently_date = $md->IndexgetData("recently_date");
        // 最近のジャンケン結果5選
        $recently_result = $md->IndexgetData("recently_result");
        $count = 0;
        foreach($recently_result as $result){
            if($result->result == "グー"){
                $recently_result_image[$count] = "<img class='goo' src='../img/goo.png' alt='グー'>";
            }
            if($result->result == "パー"){
                $recently_result_image[$count] = "<img class='par' src='../img/par.png' alt='パー'>";
            }
            if($result->result == "チョキ"){
                $recently_result_image[$count] = "<img class='choki' src='../img/choki.png' alt='チョキ'>";
            }
            $count++;
        }

        // 最近のジャンケン結果5選のURLにある動画IDのみを取得する
        for($i = 0;$i <= 4;$i++) {            
            // Youtubeの動画IDを切り出し
            $videoid[$i] = substr($recently_result[$i]->videoid, -11);
        }
          

        // ジャンケンに参戦したゲスト一覧
        $all_guest = $md->IndexgetData("all_guest");
        // ジャンケンに一番多く参戦したゲスト20人
        $most_guest = $md->IndexgetData("most_guest");

        // TODO
        /* ヒカキンじゃんけん天気予報 */
        // 予報した回数・予報が当たった回数・予報が外れた回数を取得
        $weather_summary = $md->WeatherIndexSummarygetdata();
        // 天気予報が予報した最新のじゃんけんの日付を取得
        $weather_latest_junken = $md->WeatherIndexLatestJunkengetdata();
        foreach($weather_latest_junken as $d) {
            $weather_result = $d->weather_result;
            // グー
            if($weather_result == 0){
                $latest_weather_result_image = "<img class='goo' src='../img/goo.png'>";
            }
            // パー
            if($weather_result == 1){
                $latest_weather_result_image = "<img class='par' src='../img/par.png'>";
            }
            // チョキ
            if($weather_result == 2){
                $latest_weather_result_image = "<img class='choki' src='../img/choki.png'>";
            }
        }
        // 天気予報が予報した最新のじゃんけんを取得
        $weather_latest_junken = $md->WeatherIndexLatestDategetdata();
        foreach($weather_latest_junken as $d) {
            $latest_weather_date = $d->upload_date;
        }
        // 天気予報が出した最新のじゃんけんを5件取得
        $weather_latest_junken = $md->WeatherIndexLatestWeatherJunken5getdata();
        $count = 0;
        foreach($weather_latest_junken as $d) {
            $weather_latest_junken[$count] = $d->weather_result;
            // グー
            if($weather_latest_junken[$count] == 0){
                $recently_weather_result_image[$count] = "<img class='goo' src='../img/goo.png'>";
            }
            // パー
            if($weather_latest_junken[$count] == 1){
                $recently_weather_result_image[$count] = "<img class='par' src='../img/par.png'>";
            }
            // チョキ
            if($weather_latest_junken[$count] == 2){
                $recently_weather_result_image[$count] = "<img class='choki' src='../img/choki.png'>";
            }
            // echo $recently_weather_result_image[$count];
            $count++;
        }
        // ヒカキンが出した最新のじゃんけんを5件取得
        $hikakin_latest_junken = $md->WeatherIndexLatestHikakinJunken5getdata();
        $count = 0;
        foreach($hikakin_latest_junken as $d) {
            $hikakin_latest_junken[$count] = $d->hikakin_result;
            // グー
            if($hikakin_latest_junken[$count] == 0){
                $recently_hikakin_result_image[$count] = "<img class='goo' src='../img/goo.png'>";
            }
            // パー
            if($hikakin_latest_junken[$count] == 1){
                $recently_hikakin_result_image[$count] = "<img class='par' src='../img/par.png'>";
            }
            // チョキ
            if($hikakin_latest_junken[$count] == 2){
                $recently_hikakin_result_image[$count] = "<img class='choki' src='../img/choki.png'>";
            }
            // echo $recently_hikakin_result_image[$count];
            $count++;
        }
        // 天気予報とヒカキンが出した最新のじゃんけんの日付を5件取得
        $date_latest_junken = $md->WeatherIndexLatestDate5getdata();
        $count = 0;
        foreach($date_latest_junken as $d) {
            $date_latest_junken[$count] = $d->upload_date;
            // echo $date_latest_junken[$count];
            $count++;
        }
        // 天気予報とヒカキンが出した最新のじゃんけんの手が一致するかフラグを付与する
        $count = 0;
        for($count = 0; $count < 5; $count++) {
            if($weather_latest_junken[$count] == $hikakin_latest_junken[$count]) {
                $weather_junken_match[$count] = 1;
            } else {
                $weather_junken_match[$count] = 0;
            }
        }

        // ビューを返す
        return view('index')->with([
            'total' => $total,
            'goo_total' => $goo_total,
            'par_total' => $par_total,
            'choki_total' => $choki_total,
            'recently_date' => $recently_date,
            'recently_result' => $recently_result,
            'recently_result_image' => $recently_result_image,
            'videoid' => $videoid,
            'all_guest' => $all_guest,
            'most_guest' => $most_guest,
            'weather_summary' => $weather_summary,
            'latest_weather_result_image' => $latest_weather_result_image,
            'latest_weather_date' => $latest_weather_date,
            'recently_weather_result_image' => $recently_weather_result_image,
            'recently_hikakin_result_image' => $recently_hikakin_result_image,
            'date_latest_junken' => $date_latest_junken,
            'weather_junken_match' => $weather_junken_match
        ]);
    }

    public function history(){


        // ビューを返す
        return view('history');
    }

    public function statistics(){

        // HikakinJunkenモデルのインスタンス化
        $md = new HikakinJunken();

        // 現在の年を取得
        $now_year = date('Y');

        //　年ごとのじゃんけん統計を取得
        for($year = 2014;$year <= $now_year; $year++) {
            // じゃんけんの統計をそれぞれ取得
            $year_junken[$year]['all'] = $md->YearMonthJunkengetData($year, $month=null, $status="全ての動画");
            $year_junken[$year]['junken'] = $md->YearMonthJunkengetData($year, $month=null, $status="じゃんけんのみ");
            $year_junken[$year]['goo'] = $md->YearMonthJunkengetData($year, $month=null, $status="グー");
            $year_junken[$year]['par'] = $md->YearMonthJunkengetData($year, $month=null, $status="パー");
            $year_junken[$year]['choki'] = $md->YearMonthJunkengetData($year, $month=null, $status="チョキ");
        }

        //　月ごとのじゃんけん統計を取得
        for($year = 2014;$year <= $now_year; $year++) {
            for($month = 1;$month <= 12; $month++) {
                // じゃんけんの統計をそれぞれ取得
                $month_junken[$year][$month]['all'] = $md->YearMonthJunkengetData($year, $month, $status="全ての動画");
                $month_junken[$year][$month]['junken'] = $md->YearMonthJunkengetData($year, $month, $status="じゃんけんのみ");
                $month_junken[$year][$month]['goo'] = $md->YearMonthJunkengetData($year, $month, $status="グー");
                $month_junken[$year][$month]['par'] = $md->YearMonthJunkengetData($year, $month, $status="パー");
                $month_junken[$year][$month]['choki'] = $md->YearMonthJunkengetData($year, $month, $status="チョキ");
            }
        }

        // ビューを返す
        return view('statistics')->with([
            'year_junken' => $year_junken,
            'month_junken' => $month_junken,
            'now_year' =>$now_year
        ]);
    }

    public function statistics_download(){

        // ビューを返す
        return view('statistics_download')->with([
        ]);
    }

/****
        はじめに  
****/
    public function introduction(){


        // ビューを返す
        return view('introduction')->with([
        ]);
    }

/****
        キーワード検索  
****/
    public function keyword(Request $request){

        // HikakinJunkenモデルのインスタンス化
        $md = new HikakinJunken();

        // フォームから検索するキーワードを受け取る
        $keyword = $request->input('keyword');
        // 参照ページナンバーを更新
        $page = $request->input('page');
        // 条件検索した値を受け取る
        $date = $request->input('date');
        $junken = $request->input('junken');

        // 取得するデータ範囲を設定する
        if($page == null || $page == 0) {
            $page = 1;
            $data_submin = $page * 30 - 30;
            $data_submax = $page * 30;
        } else {
            $data_submin = $page * 30 - 30;
            $data_submax = $page * 30;
        }

        if($keyword !== "" && $keyword !== null) {
            if($date != null && $junken != null){
                // 条件検索したデータ総数カウント
                $data = $md->AllCountConditionsKeywordgetData($keyword, $date, $junken);
                $search_count = count($data);
                // 条件検索付きの指定範囲のデータを取り出す
                $data_offset = $md->ConditionsOffsetKeywordgetData($keyword, $data_submin, $date, $junken);
            } else {
                // 検索したデータ総数カウント
                $data = $md->AllCountKeywordgetData($keyword);
                $search_count = count($data);
                // 指定範囲のデータを取り出す
                $data_offset = $md->OffsetKeywordgetData($keyword, $data_submin);
                $date = "asc";
                $junken = "all";
            }

            if($search_count == 0){
                // 検索結果が0件の場合エラー
                $search_error = "no result";
                $search_layout = "layouts.searcherror";
                $pagination_num = null;
            }
        } else {
            // キーワードが入力されてない場合エラー
            $data = null;
            $data_offset = null;
            $search_count = 0;
            $search_error = "no keyword";
            $search_layout = "layouts.searcherror";
            $pagination_num = null;
        }
        
        if($search_count != 0) {
            // 件数を取得できた場合
            $search_error = null;
            $search_layout = "layouts.searchdefault";
            // 検索結果の累計ページの数
            $pagination_num = ceil($search_count / 30.00);
            // 1ページにおけるデータ件数検索範囲
            $data_submin++;
            if($search_count <= $data_submax){
                $data_submax = $search_count;
            }
        }

        // ビューを返す
        return view('keyword')->with([
            'data' => $data,
            'data_offset' => $data_offset,
            'search_keyword' => $keyword,
            'search_target' => "keyword",
            'search_error' => $search_error,
            'search_count' => $search_count,
            'search_layout' => $search_layout,
            'pagination_num' => $pagination_num,
            'page' => $page,
            'data_submax' => $data_submax,
            'data_submin' => $data_submin,
            'date' => $date,
            'junken' => $junken
        ]);
    }
/****
        ゲスト検索  
****/
    public function guest($guest=null, Request $request){

        // HikakinJunkenモデルのインスタンス化
        $md = new HikakinJunken();

        // 参照ページナンバーを更新
        $page = $request->input('page');
        // 条件検索した値を受け取る
        $date = $request->input('date');
        $junken = $request->input('junken');

        // 取得するデータ範囲を設定する
        if($page == null || $page == 0) {
            $page = 1;
            $data_submin = $page * 30 - 30;
            $data_submax = $page * 30;
        } else {
            $data_submin = $page * 30 - 30;
            $data_submax = $page * 30;
        }

        if($guest !== "" && $guest !== null) {
            if($date != null && $junken != null){
                // 条件検索したデータ総数カウント
                $data = $md->AllCountConditionsGuestgetData($guest, $date, $junken);
                $search_count = count($data);
                // 条件検索付きの指定範囲のデータを取り出す
                $data_offset = $md->ConditionsOffsetGuestgetData($guest, $data_submin, $date, $junken);
            } else {
                // 検索したデータ総数カウント
                $data = $md->AllCountGuestgetData($guest);
                $search_count = count($data);
                // 指定範囲のデータを取り出す
                $data_offset = $md->OffsetGuestgetData($guest, $data_submin);
                $date = "asc";
                $junken = "all";
            }
        } else {
            // キーワードが入力されてない場合エラー
            $data = null;
            $data_offset = null;
            $search_error = "no guest";
            $search_count = 0;
            $search_layout = "layouts.searcherror";
        }

        if($search_count != 0) {
            // 件数を取得できた場合
            $search_error = null;
            $search_layout = "layouts.searchdefault";
            // 検索結果の累計ページの数
            $pagination_num = ceil($search_count / 30.00);
            // 1ページにおけるデータ件数検索範囲
            $data_submin++;
            if($search_count <= $data_submax){
                $data_submax = $search_count;
            }
        } else {
            // 検索結果が0件の場合エラー
            $search_error = "no guest";
            $search_count = 0;
            $search_layout = "layouts.searcherror";
            $pagination_num = null;
        }

        // ジャンケンに参戦したゲスト一覧
        $all_guest = $md->IndexgetData("all_guest");
        // ジャンケンに一番多く参戦したゲスト20人
        $most_guest = $md->IndexgetData("most_guest");
    
        // ビューを返す
        return view('guest')->with([
            'data' => $data,
            'data_offset' => $data_offset,
            'search_guest' => $guest,
            'all_guest' => $all_guest,
            'most_guest' => $most_guest,
            'search_target' => "guest",
            'search_error' => $search_error,
            'search_count' => $search_count,
            'search_layout' => $search_layout,
            'pagination_num' => $pagination_num,
            'page' => $page,
            'data_submax' => $data_submax,
            'data_submin' => $data_submin,
            'date' => $date,
            'junken' => $junken
        ]);
    }
/****
        日付検索  
****/
    public function date($year=null, $month=null, Request $request){
        // HikakinJunkenモデルのインスタンス化
        $md = new HikakinJunken();
    
        // 参照ページナンバーを更新
        $page = $request->input('page');
        // 条件検索した値を受け取る
        $date = $request->input('date');
        $junken = $request->input('junken');
                
        // 月が9月以下の場合、数字の前に0をつける
        if($month < 10 && $month != null){
            $month = "0" . $month;
            $type = "month";
        } else {
            $type = "year";
        }
        
        // 取得するデータ範囲を設定する
        if($page == null || $page == 0) {
            $page = 1;
            $data_submin = $page * 30 - 30;
            $data_submax = $page * 30;
        } else {
            $data_submin = $page * 30 - 30;
            $data_submax = $page * 30;
        }

        if($year !== "" && $year !== null) {
            if($date != null && $junken != null){
                // 条件検索したデータ総数カウント
                $data = $md->AllCountConditionsDategetData($year, $month, $date, $junken);
                $search_count = count($data);
                // 条件検索付きの指定範囲のデータを取り出す
                $data_offset = $md->ConditionsOffsetDategetData($year, $month, $data_submin, $date, $junken);
            } else {
                // 検索したデータ総数カウント
                $data = $md->AllCountDategetData($year, $month);
                $search_count = count($data);
                // 指定範囲のデータを取り出す
                $data_offset = $md->OffsetDategetData($year, $month, $data_submin);
                $date = "asc";
                $junken = "all";
            }
        } else {
            // キーワードが入力されてない場合エラー
            $data = null;
            $data_offset = null;
            $search_error = "no date";
            $search_count = 0;
            $search_layout = "layouts.searcherror";
        }

        // 検索結果の累計ページの数・1ページおきの件数
        if($search_count != 0){
            $search_error = null;
            $search_layout = "layouts.searchdefault";
            $pagination_num = ceil($search_count / 30.00);
            // 1ページあたりのデータ件数検索範囲を設定
            $data_submin++;
            if($search_count <= $data_submax){
                $data_submax = $search_count;
            }
        } else {
            $search_error = "no date";
            $search_count = 0;
            $search_layout = "layouts.searcherror";
            $pagination_num = null;
        }

        // 01や04のような0が付与されている月の0を切り取る
        if($month < 10 && $month != null) {
            $month = substr($month, -1);
        }

        // ビューを返す
        return view('date')->with([
            'data' => $data,
            'data_offset' => $data_offset,
            'type' => $type,
            'year' => $year,
            'month' => $month,
            'search_target' => "date",
            'search_error' => $search_error,
            'search_count' => $search_count,
            'search_layout' => $search_layout,
            'pagination_num' => $pagination_num,
            'page' => $page,
            'data_submax' => $data_submax,
            'data_submin' => $data_submin,
            'date' => $date,
            'junken' => $junken
        ]);
    }


}
