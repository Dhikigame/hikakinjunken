<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\HikakinJunken;
use App\Models\HikakinJunkenTags;

class VideoViewController extends Controller
{

    private function apikey() {
        $apikey = "AIzaSyDmxgobCWW3qLaOprjhiERlY0kfQ2fZO7w";
        return $apikey;
    }

    // 動画再生ページ
    public function watch(Request $request) {

        // HikakinJunkenモデルのインスタンス化
        $md = new HikakinJunken();

        // 動画IDを受け取る
        $videoid = $request->input('v');
        // タグを受け取る
        $tag = $request->input('tag');
        // タグの登録ボタンを押下したかどうかの判定を受け取る
        $register = $request->input('register');
        // タグの消去を受け付けたときのタグ番号を受け取る
        $delete = $request->input('delete');
        echo $delete;

        //　デフォルトの動画再生ページレイアウト
        $watch_layout = "layouts.watchdefault";

        // 動画再生用フレーム作成
        $view_info_url = "https://www.googleapis.com/youtube/v3/videos?";
        $view_info_url .= "part=statistics";
        $view_info_url .= "&id=".$videoid;
        $view_info_url .= "&fields=items%2Fstatistics";
        $view_info_url .= "&key=".$this->apikey();

        // Youtube APIより動画の再生回数取得
        // FIX: 再生回数はWebサイトに表示させない&Youtube APIのクォーター制限にかかる可能性があるため修正が必要
        // $json = file_get_contents($view_info_url);
        // $json = mb_convert_encoding($json, 'UTF8', 'ASCII,JIS,UTF-8,EUC-JP,SJIS-WIN');
        // $arr = json_decode($json, true);
        // $view_counter = $arr["items"][0]["statistics"]["viewCount"];

        // 再生動画の動画情報取得
        $viewvideo = $md->WatchgetData($videoid);

        // 過去と未来にアップされている動画1件情報取得
        $beforevideo = $md->BeforeDategetData($videoid);
        $aftervideo = $md->AfterDategetData($videoid);

        // 動画ページにおけるジャンケンの手
        $junken = $md->JunkengetData($videoid);
        if($junken == "グー"){
            $junken = "<img class='goo' src='../img/goo.png'>";
        }
        if($junken == "パー"){
            $junken = "<img class='par' src='../img/par.png'>";
        }
        if($junken == "チョキ"){
            $junken = "<img class='choki' src='../img/choki.png'>";
        }

        /* タグ機能 */
        // タグの登録ボタンが押されたら、タグを登録する
        if($register === "register") {
            $this->tagstore($tag, $videoid);
            return redirect('watch?v='.$videoid);
        } 
        // 登録されているタグを取得する
        $tag_data = $this->tagretrieve($videoid);
        $tags = $tag_data[0];
        // タグの消去受け付けたらタグを消去する
        if($delete != null){
            $this->tagdelete($delete, $videoid);
            return redirect('watch?v='.$videoid);
        }

        /* ゲスト一覧 */
        $guest = $md->GuestgetData($videoid);
        // var_dump($guest);

        /* コメント機能 */
        // TODO
 
        // ビューを返す
        return view('watch')->with([
            'watch_layout' => $watch_layout,
            'videoid' => $videoid,
            // 'view_counter' => $view_counter,
            'viewvideo' => $viewvideo,
            'beforevideo' => $beforevideo,
            'aftervideo' => $aftervideo,
            'junken' => $junken,
            'tags' => $tags,
            'guest' => $guest
        ]);
    }

    /* タグを1件登録 */
    private function tagstore($tag, $videoid) {

        // HikakinJunkenモデルのインスタンス化
        $md = new HikakinJunkenTags();

        // $tagに文字が入力されていれば、タグを登録
        // 入力値なしならば、エラー画面表示
        if($tag != null) {
            $md->TagStore($tag, $videoid);
        } else {
            // TODO:エラー画面表示
        }

        return 0;
    }

    /* タグを1件消去 */
    private function tagdelete($delete, $videoid) {

        // HikakinJunkenモデルのインスタンス化
        $md = new HikakinJunkenTags();

        // TODO: モデル内でタグの消去処理を行う
        $md->TagDelete($delete, $videoid);

        return 0;
    }

    /* タグを1件取得 */
    private function tagretrieve($videoid) {

        // HikakinJunkenモデルのインスタンス化
        $md = new HikakinJunkenTags();

        $tag_data = $md->TagRetrieve($videoid);

        return $tag_data;
    }
}
