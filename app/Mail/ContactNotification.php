<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactNotification extends Mailable
{
    use Queueable, SerializesModels;

    protected $title;
    protected $text;
    protected $email;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name, $text, $email)
    {
        //
        // echo "ContactNotification__construct";
        $this->title = sprintf('%sさんからの問い合わせ', $name);
        $this->text = $text;
        $this->email = $email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // echo "通ったbuild";

        //　デフォルトのお問い合わせページレイアウト
        $contact_layout = "layouts.contactdefault";
        //->view('emails.sample_notification')

        return $this
                ->from($this->email)
                ->text('emails.sample_notification_plain')
                ->subject($this->title)
                ->with([
                    'text' => $this->text
                ]);
    }
}
