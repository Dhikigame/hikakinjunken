<?php

namespace App\HikakinJunken;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    //
  protected $fillable = ['body'];

  // $comment->hikakin_junken_data
  public function hikakin_junken() {
    return $this->belongsTo('HikakinJunken');
  }
}