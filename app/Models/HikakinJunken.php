<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class HikakinJunken extends Model{
    // ヒカキンのジャンケン結果一覧テーブル
    protected $hikakin_junken_data = 'hikakin_junken_data';
    // 動画IDごとのタグ一覧テーブル
    protected $hikakin_junken_tags = 'hikakin_junken_tags';
    // ヒカキンじゃんけんの天気予報一覧テーブル
    protected $hikakin_junken_weather = 'hikakin_junken_weather';

    // ジャンケンゲスト参戦回数テーブル
    protected $guest_sum = 'guest_sum';
    // 動画コメントテーブル
    protected $comments = 'comments';

    public $timestamps = false;

    protected $fillable = ['body'];

    public function comments() {
      return $this->hasMany('Comment');
    }

    private function comments_db() {
        $query = DB::table($this->comments);
        return $query;
    }

    private function db() {
        $query = DB::table($this->hikakin_junken_data);
        return $query;
    }

    private function weather_db() {
        $query = DB::table($this->hikakin_junken_weather);
        return $query;
    }

    // ゲスト検索用クエリビルダの構築
    private function guest_query($query, $keyword=null, $guest=null) {
        $query = $this->db()
        ->where('title', 'LIKE', "%$keyword%")
        ->orwhere('note', 'LIKE', "%$keyword%")
        ->orwhere('guest01', 'LIKE', "%$keyword%")
        ->orwhere('guest02', 'LIKE', "%$keyword%")
        ->orwhere('guest03', 'LIKE', "%$keyword%")
        ->orwhere('guest04', 'LIKE', "%$keyword%")
        ->orwhere('guest05', 'LIKE', "%$keyword%")
        ->orwhere('guest06', 'LIKE', "%$keyword%")
        ->orwhere('guest07', 'LIKE', "%$keyword%")
        ->orwhere('guest08', 'LIKE', "%$keyword%")
        ->orwhere('guest09', 'LIKE', "%$keyword%")
        ->orwhere('guest10', 'LIKE', "%$keyword%")
        ->orwhere('guest11', 'LIKE', "%$keyword%")
        ->orwhere('guest12', 'LIKE', "%$keyword%")
        ->orwhere('guest13', 'LIKE', "%$keyword%")
        ->orwhere('guest14', 'LIKE', "%$keyword%")
        ->orwhere('guest15', 'LIKE', "%$keyword%")
        ->orwhere('guest16', 'LIKE', "%$keyword%")
        ->orwhere('guest17', 'LIKE', "%$keyword%")
        ->orwhere('guest18', 'LIKE', "%$keyword%")
        ->orwhere('guest19', 'LIKE', "%$keyword%")
        ->orwhere('guest20', 'LIKE', "%$keyword%");

        return $query;
    }

    public function SearchgetData($type=null) {
        $query = $this->db();

        if($type >= 2014 && $type <= date('Y')){
            $query->where('upload_date', 'LIKE', "%$type%");
        }

        if($type == "グー" || $type == "チョキ" || $type == "パー"){
            $query->where('result', $type);
        }

        $data = $query->get();

        return $data;
    }

    public function IndexgetData($type=null) {
        $query = $this->db();
        $query_guest_sum = DB::table($this->guest_sum);

        // 全体のジャンケン総数のデータ取得
        if($type == "total"){
            $total_count = $query->where('result', "<>" , "休み")->count();
            return $total_count;
        }
        if($type == "goo_total"){
            $goo_count = $query->where('result', "グー")->count();
            return $goo_count;
        }
        if($type == "par_total"){
            $par_count = $query->where('result', "パー")->count();
            return $par_count;
        }
        if($type == "choki_total"){
            $choki_count = $query->where('result', "チョキ")->count();
            return $choki_count;
        }

        // 最新ジャンケンの日付
        if($type == "recently_date"){
            $query->select('upload_date')->orderBy('upload_date', "desc")->limit(1);
        }

        // 最近のジャンケン結果5選
        if($type == "recently_result"){
            $query->where('result', "<>" , "休み")->orderBy('upload_date', "desc")->limit(5);
            $data = $query->get();
            return $data;
        }

        // ジャンケンに参戦したゲスト一覧
        if($type == "all_guest"){
            $query_guest_sum->select('guest')->orderBy('guest', "asc");
            $data = $query_guest_sum->get();
            return $data;
        }
        // ジャンケンに一番多く参戦したゲスト20人
        if($type == "most_guest"){
            $query_guest_sum->select('guest')->orderBy('total', "desc")->limit(20);
            $data = $query_guest_sum->get();
            return $data;
        }

        $data = $query->get();

        return $data;
    }

/****
        ヒカキンじゃんけん天気予報関連
****/
    public function WeatherIndexSummarygetdata() {
        $query_weather = $this->weather_db();
        $forecast_count['total'] = $query_weather->whereNotNull('hikakin_result')->count();
        $forecast_count['win'] = $query_weather->whereColumn('hikakin_result', '=' , 'weather_result')->count();
        $forecast_count['lose'] = $forecast_count['total'] - $forecast_count['win'];
        // $forecast_count['lose'] = $query_weather->whereColumn('hikakin_result', '<>', 'weather_result')->count();
        
        return $forecast_count;
    }

    public function WeatherIndexLatestJunkengetdata() {
        $query_weather = $this->weather_db();
        $query_weather->select('weather_result')->orderBy('upload_date', "desc")->limit(1);
        $query_weather_latest_junken_weather = $query_weather->get();
        
        return $query_weather_latest_junken_weather;
    }

    public function WeatherIndexLatestDategetdata() {
        $query_weather = $this->weather_db();
        $query_weather->select('upload_date')->orderBy('upload_date', "desc")->limit(1);
        $query_weather_latest_date_weather = $query_weather->get();
        
        return $query_weather_latest_date_weather;
    }

    public function WeatherIndexLatestWeatherJunken5getdata() {
        $query_weather = $this->weather_db();
        $query_weather->select('weather_result')->whereNotNull('hikakin_result')->orderBy('upload_date', "desc")->limit(5);
        $query_weather_latest_junken_weather = $query_weather->get();
        
        return $query_weather_latest_junken_weather;
    }

    public function WeatherIndexLatestHikakinJunken5getdata() {
        $query_weather = $this->weather_db();
        $query_weather->select('hikakin_result')->whereNotNull('hikakin_result')->orderBy('upload_date', "desc")->limit(5);
        $query_weather_latest_junken_weather = $query_weather->get();
        
        return $query_weather_latest_junken_weather;
    }


    public function WeatherIndexLatestDate5getdata() {
        $query_weather = $this->weather_db();
        $query_weather->select('upload_date')->whereNotNull('hikakin_result')->orderBy('upload_date', "desc")->limit(5);
        $query_weather_latest_date_weather = $query_weather->get();
        
        return $query_weather_latest_date_weather;
    }

/****
        キーワード検索  
****/
    public function AllCountKeywordgetData($keyword=null) {

        $query = $this->db();

        $query->join($this->hikakin_junken_tags, $this->hikakin_junken_tags.'.videoid', '=', $this->hikakin_junken_data.'.videoid');

        $query->where($this->hikakin_junken_data.'.title', 'LIKE', "%$keyword%")
        ->orwhere($this->hikakin_junken_data.'.note', 'LIKE', "%$keyword%");
        for($guest_count = 1; $guest_count <= 20; $guest_count++) {
            $tmp = $guest_count;
            if($guest_count <= 9) {
                $guest_count = "0" . (string)$guest_count;
            }
            $query->orwhere($this->hikakin_junken_data.'.guest'.$guest_count, 'LIKE', "%$keyword%");
            $guest_count = $tmp;
        }
        $query->orwhere($this->hikakin_junken_tags.'.tag1', 'LIKE', "%$keyword%");
        $query->orwhere($this->hikakin_junken_tags.'.tag2', 'LIKE', "%$keyword%");
        $query->orwhere($this->hikakin_junken_tags.'.tag3', 'LIKE', "%$keyword%");
        $query->orwhere($this->hikakin_junken_tags.'.tag4', 'LIKE', "%$keyword%");
        $query->orwhere($this->hikakin_junken_tags.'.tag5', 'LIKE', "%$keyword%");

        $data = $query->get();

        return $data;
    }
    public function OffsetKeywordgetData($keyword=null, $offset=0){

        $query = $this->db();

        $query->join($this->hikakin_junken_tags, $this->hikakin_junken_tags.'.videoid', '=', $this->hikakin_junken_data.'.videoid');

        $query->where($this->hikakin_junken_data.'.title', 'LIKE', "%$keyword%")
        ->orwhere($this->hikakin_junken_data.'.note', 'LIKE', "%$keyword%");
        for($guest_count = 1; $guest_count <= 20; $guest_count++) {
            $tmp = $guest_count;
            if($guest_count <= 9) {
                $guest_count = "0" . (string)$guest_count;
            }
            $query->orwhere($this->hikakin_junken_data.'.guest'.$guest_count, 'LIKE', "%$keyword%");
            $guest_count = $tmp;
        }
        $query->orderBy($this->hikakin_junken_data.'.upload_date', "asc")
        ->offset($offset)->limit(30);

        $query->orwhere($this->hikakin_junken_tags.'.tag1', 'LIKE', "%$keyword%");
        $query->orwhere($this->hikakin_junken_tags.'.tag2', 'LIKE', "%$keyword%");
        $query->orwhere($this->hikakin_junken_tags.'.tag3', 'LIKE', "%$keyword%");
        $query->orwhere($this->hikakin_junken_tags.'.tag4', 'LIKE', "%$keyword%");
        $query->orwhere($this->hikakin_junken_tags.'.tag5', 'LIKE', "%$keyword%");

        $data = $query->get();

        return $data;
    }
    public function AllCountConditionsKeywordgetData($keyword=null, $date="asc", $junken="all"){

        $query = $this->db();

        $query->join($this->hikakin_junken_tags, $this->hikakin_junken_tags.'.videoid', '=', $this->hikakin_junken_data.'.videoid');

        if($junken == "all"){
            $query->where($this->hikakin_junken_data.'.title', 'LIKE', "%$keyword%")
            ->orwhere($this->hikakin_junken_data.'.note', 'LIKE', "%$keyword%");
            for($guest_count = 1; $guest_count <= 20; $guest_count++) {
                $tmp = $guest_count;
                if($guest_count <= 9) {
                    $guest_count = "0" . (string)$guest_count;
                }
                $query->orwhere($this->hikakin_junken_data.'.guest'.$guest_count, 'LIKE', "%$keyword%");
                $guest_count = $tmp;
            }
        } else {
            $query->where($this->hikakin_junken_data.'.result', $junken)
            ->where(function($query) use($keyword)
            {
                $query->where($this->hikakin_junken_data.'.title', 'LIKE', "%$keyword%")
                ->orwhere($this->hikakin_junken_data.'.note', 'LIKE', "%$keyword%");
                for($guest_count = 1; $guest_count <= 20; $guest_count++) {
                    $tmp = $guest_count;
                    if($guest_count <= 9) {
                        $guest_count = "0" . (string)$guest_count;
                    }
                    $query->orwhere($this->hikakin_junken_data.'.guest'.$guest_count, 'LIKE', "%$keyword%");
                    $guest_count = $tmp;
                }
            });
        }
        $query->orwhere($this->hikakin_junken_tags.'.tag1', 'LIKE', "%$keyword%");
        $query->orwhere($this->hikakin_junken_tags.'.tag2', 'LIKE', "%$keyword%");
        $query->orwhere($this->hikakin_junken_tags.'.tag3', 'LIKE', "%$keyword%");
        $query->orwhere($this->hikakin_junken_tags.'.tag4', 'LIKE', "%$keyword%");
        $query->orwhere($this->hikakin_junken_tags.'.tag5', 'LIKE', "%$keyword%");

        $data = $query->get();

        return $data;     
    }
    public function ConditionsOffsetKeywordgetData($keyword=null, $offset=0, $date="asc", $junken="all"){

        $query = $this->db();

        $query->join($this->hikakin_junken_tags, $this->hikakin_junken_tags.'.videoid', '=', $this->hikakin_junken_data.'.videoid');
        
        if($junken == "all"){
            $query->where($this->hikakin_junken_data.'.title', 'LIKE', "%$keyword%")
            ->orwhere($this->hikakin_junken_data.'.note', 'LIKE', "%$keyword%");
            for($guest_count = 1; $guest_count <= 20; $guest_count++) {
                $tmp = $guest_count;
                if($guest_count <= 9) {
                    $guest_count = "0" . (string)$guest_count;
                }
                $query->orwhere($this->hikakin_junken_data.'.guest'.$guest_count, 'LIKE', "%$keyword%");
                $guest_count = $tmp;
            }
            $query->orwhere($this->hikakin_junken_tags.'.tag1', 'LIKE', "%$keyword%");
            $query->orwhere($this->hikakin_junken_tags.'.tag2', 'LIKE', "%$keyword%");
            $query->orwhere($this->hikakin_junken_tags.'.tag3', 'LIKE', "%$keyword%");
            $query->orwhere($this->hikakin_junken_tags.'.tag4', 'LIKE', "%$keyword%");
            $query->orwhere($this->hikakin_junken_tags.'.tag5', 'LIKE', "%$keyword%");

            $query->orderBy($this->hikakin_junken_data.'.upload_date', $date)
            ->offset($offset)->limit(30);
        } else {
            $query->where($this->hikakin_junken_data.'.result', $junken)
            ->where(function($query) use($keyword)
            {
                $query->where($this->hikakin_junken_data.'.title', 'LIKE', "%$keyword%")
                ->orwhere($this->hikakin_junken_data.'.note', 'LIKE', "%$keyword%");
                for($guest_count = 1; $guest_count <= 20; $guest_count++) {
                    $tmp = $guest_count;
                    if($guest_count <= 9) {
                        $guest_count = "0" . (string)$guest_count;
                    }
                    $query->orwhere($this->hikakin_junken_data.'.guest'.$guest_count, 'LIKE', "%$keyword%");
                    $guest_count = $tmp;
                }
                $query->orwhere($this->hikakin_junken_tags.'.tag1', 'LIKE', "%$keyword%");
                $query->orwhere($this->hikakin_junken_tags.'.tag2', 'LIKE', "%$keyword%");
                $query->orwhere($this->hikakin_junken_tags.'.tag3', 'LIKE', "%$keyword%");
                $query->orwhere($this->hikakin_junken_tags.'.tag4', 'LIKE', "%$keyword%");
                $query->orwhere($this->hikakin_junken_tags.'.tag5', 'LIKE', "%$keyword%");
            })
            ->orderBy($this->hikakin_junken_data.'.upload_date', $date)
            ->offset($offset)->limit(30);           
        }

        $data = $query->get();

        return $data;
    }

/****
        ゲスト検索  
****/
    public function AllCountGuestgetData($guest=null){

        $query = $this->db();

        for($guest_count = 1; $guest_count <= 20; $guest_count++) {
            $tmp = $guest_count;
            if($guest_count <= 9) {
                $guest_count = "0" . (string)$guest_count;
            }
            $query->orwhere('guest'.$guest_count, 'LIKE', "%$guest%");
            $guest_count = $tmp;
        }

        $data = $query->get();

        return $data;
    }
    public function OffsetGuestgetData($guest=null, $offset=0){

        $query = $this->db();

        for($guest_count = 1; $guest_count <= 20; $guest_count++) {
            $tmp = $guest_count;
            if($guest_count <= 9) {
                $guest_count = "0" . (string)$guest_count;
            }
            $query->orwhere('guest'.$guest_count, 'LIKE', "%$guest%");
            $guest_count = $tmp;
        }
        $query->orderBy('upload_date', "asc")
        ->offset($offset)->limit(30);

        $data = $query->get();

        return $data;
    }
    public function AllCountConditionsGuestgetData($guest=null, $date="asc", $junken="all"){

        $query = $this->db();

        if($junken == "all"){
            for($guest_count = 1; $guest_count <= 20; $guest_count++) {
                $tmp = $guest_count;
                if($guest_count <= 9) {
                    $guest_count = "0" . (string)$guest_count;
                }
                $query->orwhere('guest'.$guest_count, 'LIKE', "%$guest%");
                $guest_count = $tmp;
            }
        } else {
            $query->where('result', $junken)
            ->where(function($query) use($guest)
            {
                for($guest_count = 1; $guest_count <= 20; $guest_count++) {
                    $tmp = $guest_count;
                    if($guest_count <= 9) {
                        $guest_count = "0" . (string)$guest_count;
                    }
                    $query->orwhere('guest'.$guest_count, 'LIKE', "%$guest%");
                    $guest_count = $tmp;
                }
            });           
        }

        $data = $query->get();

        return $data;     
    }
    public function ConditionsOffsetGuestgetData($guest=null, $offset=0, $date="asc", $junken="all"){

        $query = $this->db();

        if($junken == "all"){
            for($guest_count = 1; $guest_count <= 20; $guest_count++) {
                $tmp = $guest_count;
                if($guest_count <= 9) {
                    $guest_count = "0" . (string)$guest_count;
                }
                $query->orwhere('guest'.$guest_count, 'LIKE', "%$guest%");
                $guest_count = $tmp;
            }
            $query->orderBy('upload_date', $date)
            ->offset($offset)->limit(30); 
        } else {
            $query->where('result', $junken)
            ->where(function($query) use($guest)
            {
                for($guest_count = 1; $guest_count <= 20; $guest_count++) {
                    $tmp = $guest_count;
                    if($guest_count <= 9) {
                        $guest_count = "0" . (string)$guest_count;
                    }
                    $query->orwhere('guest'.$guest_count, 'LIKE', "%$guest%");
                    $guest_count = $tmp;
                }
            })
            ->orderBy('upload_date', $date)
            ->offset($offset)->limit(30); 
        }

        $data = $query->get();

        return $data;
    }
/****
        日付検索  
****/
    public function AllCountDategetData($year=null, $month=null){
        $query = $this->db();

        if($year != null && $month != null){
            $query->where('upload_date', 'LIKE', "$year-$month%")->count('videoid');
        } else{
            $query->where('upload_date', 'LIKE', "$year%")->count('videoid');
        }

        $data = $query->get();

        return $data;
    }
    public function OffsetDategetData($year=null, $month=null, $offset=0){
        $query = $this->db();

        if($year != null && $month != null){
            $query->where('upload_date', 'LIKE', "$year-$month%")->orderBy('upload_date', "asc")->offset($offset)->limit(30);
        } else{
            $query->where('upload_date', 'LIKE', "$year%")->orderBy('upload_date', "asc")->offset($offset)->limit(30);
        }

        $data = $query->get();

        return $data;
    }
    public function AllCountConditionsDategetData($year=null, $month=null, $date="asc", $junken="all"){
        $query = $this->db();
        if($junken == "all" && $month != null) {
            $query->where('upload_date', 'LIKE', "$year-$month%")->orderBy('upload_date', $date);
        } 
        else if($junken == "all" && $month == null) {
            $query->where('upload_date', 'LIKE', "$year%")->orderBy('upload_date', $date);           
        }
        else if($junken != "all" && $month != null) {
            $query->where('result', $junken)->where('upload_date', 'LIKE', "$year-$month%")->orderBy('upload_date', $date);
        } else {
            $query->where('result', $junken)->where('upload_date', 'LIKE', "$year%")->orderBy('upload_date', $date);
        }

        $data = $query->get();

        return $data;     
    }
    public function ConditionsOffsetDategetData($year=null, $month=null, $offset=0, $date="asc", $junken="all"){
        $query = $this->db();
        if($junken == "all" && $month != null) {
            $query->where('upload_date', 'LIKE', "$year-$month%")->orderBy('upload_date', $date)->offset($offset)->limit(30);
        } 
        else if($junken == "all" && $month == null) {
            $query->where('upload_date', 'LIKE', "$year%")->orderBy('upload_date', $date)->offset($offset)->limit(30);        
        }
        else if($junken != "all" && $month != null) {
            $query->where('result', $junken)->where('upload_date', 'LIKE', "$year-$month%")->orderBy('upload_date', $date)->offset($offset)->limit(30);
        } else {
            $query->where('result', $junken)->where('upload_date', 'LIKE', "$year%")->orderBy('upload_date', $date)->offset($offset)->limit(30);
        }

        $data = $query->get();

        return $data;     
    }


/****
        動画再生ページ
****/
    /* 再生動画情報を取得 */
    public function WatchgetData($videoid) {

        $video_query = $this->db();
        // $video_query->where('videoid', 'LIKE', "%$videoid%");
        $video_query->where('videoid', 'LIKE', "%$videoid%");
        $data = $video_query->get();
        foreach($data as $d) {
            $viewvideo['id'] = $d->id;
            $viewvideo['title'] = $d->title;
            $viewvideo['videoid'] = $d->videoid;
            $viewvideo['uploaddate'] = $d->upload_date;
            $viewvideo['note'] = $d->note;

            return $viewvideo;
        }
        $viewvideo['id'] = "";
        $viewvideo['title'] = "";
        $viewvideo['videoid'] = "";
        $viewvideo['uploaddate'] = "";
        $viewvideo['note'] = "";

        return $viewvideo;
    }
    /* 過去の動画情報を1件取得 */
    public function BeforeDategetData($videoid) {
        // 再生する動画の投稿日時を取得
        $videodate_query = $this->db();
        $videodate_query->where('videoid', 'LIKE', "%$videoid%")->limit(1);
        $data = $videodate_query->get();
        foreach($data as $d) {
            $upload_date = $d->upload_date;
        }
        // 投稿日時の前後の動画の投稿日付,URL,動画タイトルを取得
        $videobeforedate_query = $this->db();
        $videobeforedate_query->whereDate('upload_date', '<', $upload_date)->orderBy('upload_date', 'desc')->limit(1);
        $data = $videobeforedate_query->get();
        foreach($data as $d) {
            $beforevideo['id'] = $d->id;
            $beforevideo['uploaddate'] = $d->upload_date;
            $beforevideo['videoid'] = substr($d->videoid, -11);
            $beforevideo['title'] = $d->title;

            return $beforevideo;
        }
        $viewvideo['id'] = "";
        $beforevideo['uploaddate'] = "";
        $beforevideo['videoid'] = "";
        $beforevideo['title'] = "";

        return $beforevideo;
    }
    /* 未来の動画情報を1件取得 */
    public function AfterDategetData($videoid){
        // 再生する動画の投稿日時を取得
        $videodate_query = $this->db();
        $videodate_query->where('videoid', 'LIKE', "%$videoid%")->limit(1);
        $data = $videodate_query->get();
        foreach($data as $d) {
            $upload_date = $d->upload_date;
        }
        // 投稿日時の未来の動画の投稿日付,URL,動画タイトルを取得
        $videoafterdate_query = $this->db();
        $videoafterdate_query->whereDate('upload_date', '>', $upload_date)->orderBy('upload_date', 'asc')->limit(1);
        $data = $videoafterdate_query->get();
        foreach($data as $d) {
            $viewvideo['id'] = $d->id;
            $aftervideo['uploaddate'] = $d->upload_date;
            $aftervideo['videoid'] = substr($d->videoid, -11);
            $aftervideo['title'] = $d->title;

            return $aftervideo;
        }
        $viewvideo['id'] = "";
        $aftervideo['uploaddate'] = "";
        $aftervideo['videoid'] = "";
        $aftervideo['title'] = "";

        return $aftervideo;
    }
    
    public function YearMonthJunkengetData($year=null, $month=null, $status=null){
        $query = $this->db();

        if($month >= 1 && $month <= 9) {
            $month = "0" . $month;
        }

        if($status == "全ての動画" && $month == null) {
            $query->distinct()->where('upload_date', 'LIKE', "$year%")->count("videoid");
        }
        if($status == "じゃんけんのみ" && $month == null) {
            $query->where('upload_date', 'LIKE', "$year%")->where('result', "<>" , "休み")->count("videoid");
        }
        if($status == "グー" && $month == null) {
            $query->where('upload_date', 'LIKE', "$year%")->where('result', "グー")->count("videoid");
        }
        if($status == "パー" && $month == null) {
            $query->where('upload_date', 'LIKE', "$year%")->where('result', "パー")->count("videoid");
        }
        if($status == "チョキ" && $month == null) {
            $query->where('upload_date', 'LIKE', "$year%")->where('result', "チョキ")->count("videoid");
        }

        if($status == "全ての動画" && $month != null) {
            $query->distinct()->where('upload_date', 'LIKE', "$year-$month%")->count("videoid");
        }
        if($status == "じゃんけんのみ" && $month != null) {
            $query->where('upload_date', 'LIKE', "$year-$month%")->where('result', "<>" , "休み")->count("videoid");
        }
        if($status == "グー" && $month != null) {
            $query->where('upload_date', 'LIKE', "$year-$month%")->where('result', "グー")->count("videoid");
        }
        if($status == "パー" && $month != null) {
            $query->where('upload_date', 'LIKE', "$year-$month%")->where('result', "パー")->count("videoid");
        }
        if($status == "チョキ" && $month != null) {
            $query->where('upload_date', 'LIKE', "$year-$month%")->where('result', "チョキ")->count("videoid");
        }
        
        $data = $query->get();

        return $data;
    }

    /* 動画で行われたジャンケン結果を取得 */
    public function JunkengetData($videoid){
        $query = $this->db();

        $query->where('videoid', 'LIKE', "%$videoid%")->limit(1);
        $data = $query->get();
        foreach($data as $d){
            $junken = $d->result;
        }

        return $junken;
    }

    /* 動画内の参戦ゲストを取得 */
    public function GuestgetData($videoid){

        $query = $this->db()
        ->where('videoid', 'LIKE', "%$videoid%")->limit(1);
        $data = $query->get();

        foreach($data as $d){
            $guest[0] = $d->guest01;
            $guest[1] = $d->guest02;
            $guest[2] = $d->guest03;
            $guest[3] = $d->guest04;
            $guest[4] = $d->guest05;
            $guest[5] = $d->guest06;
            $guest[6] = $d->guest07;
            $guest[7] = $d->guest08;
            $guest[8] = $d->guest09;
            $guest[9] = $d->guest10;
            $guest[10] = $d->guest11;
            $guest[11] = $d->guest12;
            $guest[12] = $d->guest13;
            $guest[13] = $d->guest14;
            $guest[14] = $d->guest15;
            $guest[15] = $d->guest16;
            $guest[16] = $d->guest17;
            $guest[17] = $d->guest18;
            $guest[18] = $d->guest19;
            $guest[19] = $d->guest20;
        }
        
        return $guest;
    }


    /* 動画再生ページに投稿されたコメントを登録 */
    // TODO
    public function CommentStore($comment, $videoid){

        $junken_count_query = $this->db();
        $junken_count_query->select('junken_count')->where('videoid', 'LIKE', "$videoid%")->limit(1);
        $junken_count_data = $junken_count_query->get();

        $comments_query = $this->comments_db();
        $today = date("Y-m-d H:i:s");

        $comments_query->create([
            'body' => $comment,
            'created_at' => $today,
            'updated_at' => $today
        ]);
    }

}
