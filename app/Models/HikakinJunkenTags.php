<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class HikakinJunkenTags extends Model
{
    // 動画IDごとのタグ一覧テーブル
    protected $hikakin_junken_tags = 'hikakin_junken_tags';

    // DB内に同じタグがあるか判定
    private $SAME_TAG = "same_tag";
    private $NO_SAME_TAG = "no_same_tag";
    // DBへのタグ登録判定
    private $REGISTER = "register_tag";
    private $NO_REGISTER = "no_register_tag";

    private function db() {
        $query = DB::table($this->hikakin_junken_tags);
        return $query;
    }

    // 登録タグに空きがあるか検索
    // 同じタグが登録されているか調べる
    private function TagSameSearch($register_tag=null, $videoid=null) {

        $query = $this->db();

        //　タグごとに同じタグがあるか調べる
        $query->select('tag1')->where('videoid', 'LIKE', "%$videoid%");
        $tag1_data = $query->get();
        $tag1 = $tag1_data[0]->tag1;
        if ($tag1 === $register_tag) {
            return $this->SAME_TAG;
        }

        $query->select('tag2')->where('videoid', 'LIKE', "%$videoid%");
        $tag2_data = $query->get();
        $tag2 = $tag2_data[0]->tag2;
        if ($tag2 === $register_tag) {
            return $this->SAME_TAG;
        }

        $query->select('tag3')->where('videoid', 'LIKE', "%$videoid%");
        $tag3_data = $query->get();
        $tag3 = $tag3_data[0]->tag3;
        if ($tag3 === $register_tag) {
            return $this->SAME_TAG;
        }

        $query->select('tag4')->where('videoid', 'LIKE', "%$videoid%");
        $tag4_data = $query->get();
        $tag4 = $tag4_data[0]->tag4;
        if ($tag4 === $register_tag) {
            return $this->SAME_TAG;
        }

        $query->select('tag5')->where('videoid', 'LIKE', "%$videoid%");
        $tag5_data = $query->get();
        $tag5 = $tag5_data[0]->tag5;
        if ($tag5 === $register_tag) {
            return $this->NO_REGISTER;
        }

        return $this->NO_SAME_TAG;
    }
    
    // タグのカラムに空きがあれば登録する　なければ、エラー判定を返す
    private function TagNullSearch($register_tag=null, $videoid=null) {

        $query = $this->db();

        // タグごとに存在するか確認し、なければタグを登録する
        $query->select('tag1')->where('videoid', 'LIKE', "%$videoid%");
        $tag1_data = $query->get();
        $tag1 = $tag1_data[0]->tag1;
        if ($tag1 == null || $tag1 == $register_tag) {
            $query->where('videoid', $videoid)
                  ->update(['tag1' => $register_tag]); 
            return $this->REGISTER;
        }

        $query->select('tag2')->where('videoid', 'LIKE', "%$videoid%");
        $tag2_data = $query->get();
        $tag2 = $tag2_data[0]->tag2;
        if ($tag2 == null || $tag2 == $register_tag) {
            $query->where('videoid', $videoid)
                  ->update(['tag2' => $register_tag]); 
            return $this->REGISTER;
        }

        $query->select('tag3')->where('videoid', 'LIKE', "%$videoid%");
        $tag3_data = $query->get();
        $tag3 = $tag3_data[0]->tag3;
        if ($tag3 == null || $tag3 == $register_tag) {
            $query->where('videoid', $videoid)
                  ->update(['tag3' => $register_tag]); 
            return $this->REGISTER;
        }

        $query->select('tag4')->where('videoid', 'LIKE', "%$videoid%");
        $tag4_data = $query->get();
        $tag4 = $tag4_data[0]->tag4;
        if ($tag4 == null || $tag4 == $register_tag) {
            $query->where('videoid', $videoid)
                  ->update(['tag4' => $register_tag]); 
            return $this->REGISTER;
        }

        $query->select('tag5')->where('videoid', 'LIKE', "%$videoid%");
        $tag5_data = $query->get();
        $tag5 = $tag5_data[0]->tag5;
        if ($tag5 == null) {
            $query->where('videoid', $videoid)
                  ->update(['tag5' => $register_tag]); 
            return $this->REGISTER;
        }

        return $this->NO_REGISTER;
    }

    /* タグカラム群の歯抜け埋めを登録する */
    private function TagSortRegistMerge($tags_array=null, $videoid=null, $delete=null) {

        $query = $this->db();

        $tag_number = (int)substr($delete, -1);

        while($tag_number != 5) {
            $query->where('videoid', $videoid)
                  ->update(['tag'.$tag_number => $tags_array[$tag_number]]); 
            $tag_number++;
        }
        $query->where('videoid', $videoid)
              ->update(['tag5' => '']); 

    }

    /* タグカラム群の歯抜けを詰める */
    private function TagSortMerge($videoid=null, $delete=null) {

        $query = $this->db();
        $tags_array = [null, null, null, null, null];

        $query->where('videoid', 'LIKE', "%$videoid%");
        $tags = $query->get();
        $tags_array[0] = $tags[0]->tag1;
        $tags_array[1] = $tags[0]->tag2;
        $tags_array[2] = $tags[0]->tag3;
        $tags_array[3] = $tags[0]->tag4;
        $tags_array[4] = $tags[0]->tag5;

        echo '・ソート前<br>';
        foreach($tags_array as $key => $value ){
            echo $key . ' は ' . $value . '<br>';
        }

        $this->TagSortRegistMerge($tags_array, $videoid, $delete);
    }

    /* タグを登録する(登録できない場合はエラーを返す) */
    public function TagStore($tag=null, $videoid=null) {
        echo $tag."<br>";
        echo $videoid."<br>";
        $query = $this->db();

        // 登録されているタグを検索
        // 同じタグが登録されているか調べる
        $register_result = $this->TagSameSearch($tag, $videoid);
        if($this->NO_REGISTER === $register_result) {
            return $register_result;
        }
        if($this->SAME_TAG === $register_result) {
            return $register_result;
        }
        // タグのカラムに空きがあれば登録する　なければ、エラー判定を返す
        $register_result = $this->TagNullSearch($tag, $videoid);
        if($this->REGISTER === $register_result) {
            echo "OKOKOK!!!";
        } else {
            echo "NGNGNG!!!";
        }

        return $register_result;
    }

    /* タグを消去する */
    public function TagDelete($delete=null, $videoid=null) {

        $query = $this->db();

        // TODO
        if($delete == "tag1") {
            $query->where('videoid', $videoid)
                  ->update(['tag1' => '']);
        }
        if($delete == "tag2") {
            $query->where('videoid', $videoid)
                  ->update(['tag2' => '']);
        }
        if($delete == "tag3") {
            $query->where('videoid', $videoid)
                  ->update(['tag3' => '']);
        }
        if($delete == "tag4") {
            $query->where('videoid', $videoid)
                  ->update(['tag4' => '']);
        }
        if($delete == "tag5") {
            $query->where('videoid', $videoid)
                  ->update(['tag5' => '']);
        }
        $this->TagSortMerge($videoid, $delete);

    }

    /* 動画IDから登録されているタグを取得する */
    public function TagRetrieve($videoid=null) {

        $query = $this->db();

        $query->where('videoid', 'LIKE', "%$videoid%");
        $tag_data = $query->get();

        return $tag_data;
    }
}
