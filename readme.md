# hikakinjunken

Youtuberヒカキンさんの投稿する動画で行われる最後のじゃんけんをまとめたサイトです。
動画を日付ごと、キーワード、一緒にじゃんけんしたゲストからなどで検索が可能です。
また、ヒカキンさんが投稿した最新の動画で行われるじゃんけんを予測するコーナーも設けています。

It is a site that summarizes the last rock-paper-scissors performed in the video posted by Youtuber Hikakin.
You can search for videos by date, keywords, and guests who played rock-paper-scissors together.
There is also a section for predicting rock-paper-scissors that will be played in the latest video posted by Hikakin.

# Release

https://hikakinjunken.tk/

# Requirement

- Laravel

- PHP7

- MySQL

- javascript

- HTML5

  

  ### Operating environment

  * さくらVPS

  * CentOS7 × 2(Web+API Server、DB Server) 
  
  

# Features

各ページごとに説明します。

- [トップページ](https://hikakinjunken.tk/)

- 大きく分けて、「じゃんけんの累計と最近のじゃんけん結果」「ブンブンじゃんけん天気予報」「ブンブンじゃんけん検索」「日付別じゃんけん結果」「ブンブンじゃんけんの時系列推移」となっています。

- このページから各ページに移行することができます。

- [じゃんけんの累計と最近のじゃんけん結果](https://hikakinjunken.tk/)

- トップページから確認できます。

- 今まで行われたじゃんけんの総数とグー・パー・チョキのそれぞれの累計が表示されます。

- また、最近投稿された動画5選において、じゃんけんの結果も確認できます！

- [↓結果]をクリックするとじゃんけんの結果が表示されます。(ネタバレになりますので、最初に動画を見てから確認することをオススメします。)

- [ブンブンじゃんけん天気予報](https://hikakinjunken.tk/#weather_junken)

- 運営者が制作したヒカキンさんの投稿した動画で行われるじゃんけんを予測するプログラムを運用しております。

- 毎日朝6時に何の手を出すか予報していきます！

- 当たらなくてもクレームしないでください...

- [ブンブンじゃんけん検索](https://hikakinjunken.tk/#keyword_search)

- 主に「好きなキーワードを入力して検索する方法」と「ヒカキンさんと一緒にじゃんけんしたゲスト」から検索する2種類あります。

- また、「🌟みんながよく検索するキーワード🌟」と「🌟主に出演したゲスト🌟」のようなショートカットの項目も用意されております。

- - [キーワード検索](https://hikakinjunken.tk/#keyword_search)
  - 動画のタイトル、じゃんけんしたゲスト、備考に含まれる一致するキーワードを検索します。

  - [ゲスト検索](https://hikakinjunken.tk/#guest_search)
  - ヒカキンさんと一緒にじゃんけんしたゲストをメニューから選択することで検索できます。

- 　

- [日時別ブンブンじゃんけん一覧](https://hikakinjunken.tk/#date_list)

- 年代・月ごとに行われたブンブンじゃんけんの一覧を検索できます。

- [ブンブンじゃんけんの時系列推移](https://hikakinjunken.tk/#transition)

- 今まで行われたブンブンじゃんけんのグー・パー・チョキの数の推移をグラフによって確認することができます。

- グラフを見るには、[クリックして表示↓]をクリックすることで確認できます。

- グラフ観覧中に左下のボタンをクリックすることで一時停止や初めから再生など行えます。

# Other related pages

じゃんけんの結果・統計を報告するためのTwitterBOT

Twitter BOT to report the results and statistics of rock-paper-scissors

https://twitter.com/hikakinjunken1

# Author

- Dhiki(Infrastructure engineer & Video contributor)
- https://twitter.com/DHIKI_pico