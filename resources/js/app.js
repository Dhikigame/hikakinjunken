/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});

// 名前の文字数カウンタ
var input_name = document.getElementById("NameInput");
var span_name = document.getElementById("Name_inputCounter");
// メールアドレスの文字数カウンタ
var input_mail = document.getElementById("MailInput");
var span_mail = document.getElementById("Mail_inputCounter");
// お問い合わせ内容の文字数カウンタ
var input_contact = document.getElementById("ContactInput");
var span_contact = document.getElementById("Contact_inputCounter");

input_name.addEventListener("keyup", function() {
  span_name.textContent = input_name.value.length;
  if(span_name.textContent > 50) {
    $("p.name_counter_color").css({'color': 'red'});
  }
  if(span_name.textContent <= 50) {
    $("p.name_counter_color").css({'color': 'blue'});
  }
});

input_mail.addEventListener("keyup", function() {
    span_mail.textContent = input_mail.value.length;
    if(span_mail.textContent > 100) {
      $("p.mail_counter_color").css({'color': 'red'});
    }
    if(span_mail.textContent <= 100) {
      $("p.mail_counter_color").css({'color': 'blue'});
    }
});

input_contact.addEventListener("keyup", function() {
    span_contact.textContent = input_contact.value.length;
    if(span_contact.textContent > 1000) {
      $("p.contact_counter_color").css({'color': 'red'});
    }
    if(span_contact.textContent <= 1000) {
      $("p.contact_counter_color").css({'color': 'blue'});
    }
});