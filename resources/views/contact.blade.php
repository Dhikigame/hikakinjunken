@extends($contact_layout)

@section('title', 'お問い合わせ - ヒカキンブンブンじゃんけん記録室')

@section('content')

<ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
  <li itemprop="itemListElement" itemscope
      itemtype="https://schema.org/ListItem">
    <a itemprop="item" href="/">
        <span itemprop="name">トップ</span>
    </a>
    <meta itemprop="position" content="1" />
  </li>
>
  <li itemprop="itemListElement" itemscope
      itemtype="https://schema.org/ListItem">
    <a itemprop="item" href="/contact">
        <span itemprop="name">お問い合わせ</span>
    </a>
    <meta itemprop="position" content="2" />
  </li>
</ol>

<h2 class="contact-title">お問い合わせ</h2>

<span class="error_massage">
<?php
    // エラーメッセージを表示する
    echo $name_error . "<br>";
    echo $email_error . "<br>";
    echo $contacttext_error . "<br>";
?>
</span>

<span class="mail_send_message">
<?php
    // メール送信成功のメッセージを表示する
    echo $mail_send_message . "<br>";
?>
</span>

<div class="Form">
    <form action='{{ url("/contact") }}' method='post'>
    {{ csrf_field() }}
    <div class="Form-Item">
        <p class="Form-Item-Label"><span class="Form-Item-Label-Required">必須</span>お名前</p>
        <input type="text" name='name' id="NameInput" maxlength="50" class="Form-Item-Input" placeholder="例) hikakin (50文字以内)" value="<?php echo $name; ?>">
        <p class="name_counter_color"><span id="Name_inputCounter">0</span>文字</p>
    </div>
    <div class="Form-Item">
        <p class="Form-Item-Label"><span class="Form-Item-Label-Required">必須</span>メールアドレス</p>
        <input type="email" name='mail' id="MailInput" maxlength="100" class="Form-Item-Input" placeholder="例) hogehoge@gmail.com (100文字以内)" value="<?php echo $email; ?>">
        <p class="mail_counter_color"><span id="Mail_inputCounter">0</span>文字</p>
    </div>
    <div class="Form-Item">
        <p class="Form-Item-Label isMsg"><span class="Form-Item-Label-Required">必須</span>お問い合わせ内容</p>
        <textarea class="Form-Item-Textarea" id="ContactInput" maxlength="1000" name='contact' placeholder="(1000文字以内)"><?php echo $contact_text; ?></textarea>
        <p class="contact_counter_color"><span id="Contact_inputCounter">0</span>文字</p>
    </div>
    <input type="submit" class="Form-Btn" value="送信する">
    </form>
</div>

@endsection