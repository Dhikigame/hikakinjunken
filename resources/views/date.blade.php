@extends($search_layout)

<?php
  if($month != null){
    $search_date = $year . "年" . $month . "月";
    $class_attribute_value_month = "month-selected"; 
    $class_attribute_value_year = "";
    /* 検索する日付 */
    $search_year = $year;
    $search_month = $month;
  } else {
    $search_date = $year . "年";
    $class_attribute_value_year = "year-selected"; 
    $class_attribute_value_month = ""; 
    /* 検索する日付 */
    $search_year = $year;
    $search_month = null;
    $search_month_if = "";
  }

  /* 現在の年月を取得 */
  $now_year = date('yy');
  $now_month = date('n');
  if($search_error == "no date") {
    $year = $now_year;
    $month = $now_month;
  }
?>
@if (empty($search_month))
  @section('title')
    {{$search_year}}年 (日付検索) - ヒカキンブンブンじゃんけん記録室
  @endsection
@else
  @section('title')
    {{$search_year}}年{{$search_month}}月 (日付検索) - ヒカキンブンブンじゃんけん記録室
  @endsection
@endif

@section('content')

<ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
  <li itemprop="itemListElement" itemscope
      itemtype="https://schema.org/ListItem">
    <a itemprop="item" href="/">
        <span itemprop="name">トップ</span>
    </a>
    <meta itemprop="position" content="1" />
  </li>

  <li itemprop="itemListElement" itemscope
      itemtype="https://schema.org/ListItem">
    <a itemprop="item" href="/date/<?php if($search_month != null) { echo $search_year."/".$search_month; } else { echo $search_year; } ?> ">
        <span itemprop="name">
          <?php if($search_month != null) { echo $search_year."年".$search_month."月"; } else { echo $search_year."年"; } ?> で日付検索
        </span>
    </a>
    <meta itemprop="position" content="2" />
  </li>
</ol>


<h2 class="search-title">日付検索ページ</h2>


  <?php
  echo "<div class='year'>
  <label class='".$class_attribute_value_year."' for='year'>
    <a href='".url('/date/'.$year.'/')."'>".$year."年のブンブンじゃんけん</a>
  </label>
  </div>";
  ?>
<h3 class="month_description">月別一覧</h3>
<p></p>
<div class="month-list-one">
  <?php
  if($now_year == $year && $month <= 6) {
    for($i = 1; $i <= 6; $i++){
      if($month == $i){
        echo "<label class='".$class_attribute_value_month."' for='label_month'><a href='".url('/date/'.$year.'/'.$i)."?date=".$date."&junken=".$junken."'>". $i ."月</a></label>";
      } else {
        echo "<label for='label_month'><a href='".url('/date/'.$year.'/'.$i)."?date=".$date."&junken=".$junken."'>". $i ."月</a></label>";
      }
    }
  } else {
    for($i = 1; $i <= 6; $i++){
      if($month == $i){
        echo "<label class='".$class_attribute_value_month."' for='label_month'><a href='".url('/date/'.$year.'/'.$i)."?date=".$date."&junken=".$junken."'>". $i ."月</a></label>";
      } else {
        echo "<label for='label_month'><a href='".url('/date/'.$year.'/'.$i)."?date=".$date."&junken=".$junken."'>". $i ."月</a></label>";
      }
    }
  }
  ?>
</div>
<div class="month-list-two">
  <?php
  if($now_year == $year && $month <= 12) {
    for($i = 7; $i <= 12; $i++){
      if($month == $i){
        echo "<label class='".$class_attribute_value_month."' for='label_month'><a href='".url('/date/'.$year.'/'.$i)."?date=".$date."&junken=".$junken."'>". $i ."月</a></label>";
      } else {
        echo "<label for='label_month'><a href='".url('/date/'.$year.'/'.$i)."?date=".$date."&junken=".$junken."'>". $i ."月</a></label>";
      }
    }
  } else {
    for($i = 7; $i <= 12; $i++){
      if($month == $i){
        echo "<label class='".$class_attribute_value_month."' for='label_month'><a href='".url('/date/'.$year.'/'.$i)."?date=".$date."&junken=".$junken."'>". $i ."月</a></label>";
      } else {
        echo "<label for='label_month'><a href='".url('/date/'.$year.'/'.$i)."?date=".$date."&junken=".$junken."'>". $i ."月</a></label>";
      }
    }
  }
  ?>
</div>

<div class="date-form">
<?php 
    echo '<form method="get" action="'.url('/date/'.$year.'/'.$month).'">';
?>
    <div>
      <input id="asc" type="radio" name="date" value="asc" <?php if($date == "asc"){ echo "checked";} ?> ><label for="asc">日付が古い順</label>
      <input id="desc" type="radio" name="date" value="desc" <?php if($date == "desc"){ echo "checked";} ?> ><label for="desc">日付が新しい順</label>
    </div>    
    <div>
      <input id="all" type="radio" name="junken" value="all" <?php if($junken == "all"){ echo "checked";} ?> ><label for="all">全ての手</label>
      <input id="goo" type="radio" name="junken" value="グー" <?php if($junken == "グー"){ echo "checked";} ?> ><label for="goo">グー</label>
      <input id="choki" type="radio" name="junken" value="チョキ" <?php if($junken == "チョキ"){ echo "checked";} ?> ><label for="choki">チョキ</label>
      <input id="par" type="radio" name="junken" value="パー" <?php if($junken == "パー"){ echo "checked";} ?> ><label for="par">パー</label>
      <input id="rest" type="radio" name="junken" value="休み" <?php if($junken == "休み"){ echo "checked";} ?> ><label for="rest">休み</label>
    </div>

    <div class="guest-button">
      <input class="guest-btn-square-so-pop" type="submit" value="検索">
    </div>
  </form>
</div>


<h3 class="search-count">
  <?php echo $search_date ?>に投稿された動画 全<?php echo count($data) ?>件
  <?php
  if($search_count != 0){
    echo "<span class='page-num'>(".$data_submin."〜".$data_submax."件)</span>";
  }
    $search_conditions = "(";
    if($date == "desc") { $search_conditions .= "日付が新しい順、";}
    else { $search_conditions .= "日付が古い順、";}
  
    if($junken == "all") { $search_conditions .= "ジャンケンの手：全ての手";}
    else if($junken == "グー")  { $search_conditions .= "ジャンケンの手：グー";}
    else if($junken == "チョキ")  { $search_conditions .= "ジャンケンの手：チョキ";}
    else if($junken == "パー")  { $search_conditions .= "ジャンケンの手：パー";}
    else if($junken == "休み")  { $search_conditions .= "ジャンケンが休み";}
    else { $search_conditions .= "ジャンケンの手：全ての手";}
  
    $search_conditions .= ")";
    echo $search_conditions;
  ?>
</h3>

@endsection