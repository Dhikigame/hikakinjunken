@extends('layouts.default')

@section('title', 'ヒカキンブンブンじゃんけん記録室')

@section('content')

<h2 class="contact-title">404エラー ページが見つかりません！</h2>

<h4>存在しないページにアクセスしたか、ページが移動または削除された可能性があります。</h4>

<?php
    echo "<label for='label_guest'><a href='".url('/')."'>ホームへ戻る</label>";
?>

@endsection