@extends('layouts.default')

@section('title', 'ヒカキンブンブンじゃんけん記録室')

@section('content')

<h2 class="contact-title">503エラー サーバーが一時停止しております。</h2>

<h4>サービスが過負荷やメンテナンスで使用不可能な状態です。</h4>

<?php
    echo "<label for='label_guest'><a href='".url('/')."'>ホームへ戻る</label>";
?>

@endsection