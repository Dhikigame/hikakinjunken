@extends($search_layout)

@section('title', 'ヒカキンブンブンじゃんけん記録室')

@section('content')

<ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
  <li itemprop="itemListElement" itemscope
      itemtype="https://schema.org/ListItem">
    <a itemprop="item" href="/">
        <span itemprop="name">トップ</span>
    </a>
    <meta itemprop="position" content="1" />
  </li>
>
  <li itemprop="itemListElement" itemscope
      itemtype="https://schema.org/ListItem">
    <a itemprop="item" href="/guest/<?php echo $search_guest ?> ">
        <span itemprop="name">[ <?php echo $search_guest ?> ]でゲスト検索</span>
    </a>
    <meta itemprop="position" content="2" />
  </li>
</ol>

<h2 class="search-title">ゲスト検索ページ</h2>


<!--ゲスト検索プルダウンメニュー-->
<div class="guest-form-guest-page">
  <div class="guest_label">ゲストを選んでね！</div>
  <select class="guest_iptxt" onChange="location.href=value;">
    <option class="guest_ef" value="" placeholder="ゲスト一覧 ↓"><span class="guest_name">↓ ゲスト一覧</span></option>
    @foreach($all_guest as $d)
    <?php
      if($search_guest == $d->guest) { $selected_flag = " selected";}
      else {$selected_flag = "";}
      echo "<option class='guest_ef' value='".url('/guest/'.$d->guest)."'".$selected_flag.">".$d->guest."</option>";
    ?>
    @endforeach      
  </select>
  
    <?php 
    echo '<form method="get" action="'.url('/guest/'.$search_guest).'?date='.$date.'&junken='.$junken.'">';
    ?>
    <div>
      <input id="asc" type="radio" name="date" value="asc" <?php if($date == "asc"){ echo "checked";} ?> ><label for="asc">日付が古い順</label>
      <input id="desc" type="radio" name="date" value="desc" <?php if($date == "desc"){ echo "checked";} ?> ><label for="desc">日付が新しい順</label>
    </div>    
    <div>
      <input id="all" type="radio" name="junken" value="all" <?php if($junken == "all"){ echo "checked";} ?> ><label for="all">全ての手</label>
      <input id="goo" type="radio" name="junken" value="グー" <?php if($junken == "グー"){ echo "checked";} ?> ><label for="goo">グー</label>
      <input id="choki" type="radio" name="junken" value="チョキ" <?php if($junken == "チョキ"){ echo "checked";} ?> ><label for="choki">チョキ</label>
      <input id="par" type="radio" name="junken" value="パー" <?php if($junken == "パー"){ echo "checked";} ?> ><label for="par">パー</label>
      <input id="rest" type="radio" name="junken" value="休み" <?php if($junken == "休み"){ echo "checked";} ?> ><label for="rest">休み</label>
    </div>

    <div class="guest-button">
      <input class="guest-btn-square-so-pop" type="submit" value="検索">
    </div>
  </form>
</div>
<!-- ゲスト検索プルダウンメニューここまで -->
<div class="guest-search">
  <h4>🌟主に出演したゲスト🌟</h4>
    <div class="guest-list-one">
    <?php
      for($i = 0; $i <= 4; $i++){
        echo "<label for='label_guest'><a href='".url('/guest/'.$most_guest[$i]->guest)."?date=".$date."&junken=".$junken."'>". $most_guest[$i]->guest ."</a></label>";
      }
    ?>
    </div>
    <div class="guest-list-two">
    <?php
      for($i = 5; $i <= 9; $i++){
        echo "<label for='label_guest'><a href='".url('/guest/'.$most_guest[$i]->guest)."?date=".$date."&junken=".$junken."'>". $most_guest[$i]->guest ."</a></label>";
      }
    ?>
    </div>
</div>


<h3 class="search-count">
  {{$search_guest}}さんがゲスト出演した動画 全<?php echo count($data) ?>件
  <?php
  if($search_count != 0){
    echo "<span class='page-num'>(".$data_submin."〜".$data_submax."件)</span>";
  }
    $search_conditions = "(";
    if($date == "desc") { $search_conditions .= "日付が新しい順、";}
    else { $search_conditions .= "日付が古い順、";}
  
    if($junken == "all") { $search_conditions .= "ジャンケンの手：全ての手";}
    else if($junken == "グー")  { $search_conditions .= "ジャンケンの手：グー";}
    else if($junken == "チョキ")  { $search_conditions .= "ジャンケンの手：チョキ";}
    else if($junken == "パー")  { $search_conditions .= "ジャンケンの手：パー";}
    else if($junken == "休み")  { $search_conditions .= "ジャンケンが休み";}
    else { $search_conditions .= "ジャンケンの手：全ての手";}
  
    $search_conditions .= ")";
    echo $search_conditions;
  ?>
</h3>

@endsection