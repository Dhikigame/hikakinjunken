<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <title>model sample</title>
</head>
<body>
    <ul>
    @foreach($data as $d)
      <li>{{$d->title}}</li>
      <li>{{$d->url}}</li>
      <li>{{$d->upload_date}}</li>
      <li>{{$d->result}}</li>
      <li>{{$d->junken_count}}</li>
    @endforeach
  </ul>
</body>
</html>