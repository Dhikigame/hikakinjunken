@extends('layouts.default')

@section('title', '更新履歴 - ヒカキンブンブンじゃんけん記録室')

@section('content')

<ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
  <li itemprop="itemListElement" itemscope
      itemtype="https://schema.org/ListItem">
    <a itemprop="item" href="/">
        <span itemprop="name">トップ</span>
    </a>
    <meta itemprop="position" content="1" />
  </li>
>
  <li itemprop="itemListElement" itemscope
      itemtype="https://schema.org/ListItem">
    <a itemprop="item" href="/history">
        <span itemprop="name">更新履歴</span>
    </a>
    <meta itemprop="position" content="2" />
  </li>
</ol>

<h2 class="introduction-item">更新履歴</h2>

<ul>
  <li>
    (2022/03/03)
  </li>
  あかりをつけましょぼんぼりに！<br>
  <a href="/#statistics">ブンブンじゃんけん統計データ</a>のコーナーをはじめました！<br>
  今までのブンブンじゃんけんの手の一覧を確認できたり、統計データをダウンロードすることができます。<br>
  ※以前の「年月別ブンブンじゃんけん一覧」は「統計データ一覧」と変更しております。
  <li>
    (2021/01/01)
  </li>
  あけましておめでとうございます！<br>
  <a href="/#weather_junken">ブンブンじゃんけん天気予報</a>のコーナーをはじめました！<br>
  
  <li>
    (2020/10/10)
  </li>
  同じゲストなのに名前が複数あったため修正
  <ul>
    <li>「ワタナベマホト」　と　「マホト」が重複　→　「ワタナベマホト」に統合</li>
    <li>「Kumamiki」　と　「くまみき」が重複　→　「Kumamiki(くまみき)」に統合</li>
    <li>「Masuo」　と　「マスオ」が重複　→　「マスオ(Masuo)」に統合</li>
  </ul>
  <li>
    (2020/09/24)
  </li>
  サイトをリニューアル！
</ul>


@endsection