@extends('layouts.default')


@section('title', 'ヒカキンブンブンじゃんけん記録室')


@section('content')
<main>
  <div class="main-content">


    <div class="box18">
      <div class="dtable">
        <div class="dtable_c">
          <table class='all_junken_table' border='1'>
            <tbody>
              <tr>
                <th colspan='3'  class='all_junken_exp'>@foreach($recently_date as $d){{$d->upload_date}}@endforeachまでの<br>ジャンケン数</th>
              </tr>
              <tr class='all_junken_table_cell'>
                <th colspan='3' class='all'>{{$total}}</th>
              </tr>
              <tr>
                <th>グー</th>
                <th>パー</th>
                <th>チョキ</th>
              </tr>
              <tr class='all_junken_table_cell'>
                <th class='each'>{{$goo_total}}</th>
                <th class='each'>{{$par_total}}</th>
                <th class='each'>{{$choki_total}}</th>
              </tr>
            </tbody>
          </table>
        </div>

        <div class="dtable_c">
          <table class='recently_junken_table' border='1'>
            <tbody>
              <tr>
                <th colspan='4' class='recently_junken_exp'>最新のジャンケン結果</th>
              </tr>
              <tr class='recently_junken_table_cell'>
                <?php
                  echo "<th class='thumbnail'><a href=".url('/watch/?v='.$videoid[0])."><img class='thumbnail-img' src='https://img.youtube.com/vi/".$videoid[0]."/mqdefault.jpg' alt='".$recently_result[0]->title."'></a></th>";
                ?>
                <th class='upload_date'>{{$recently_result[0]->upload_date}}</th>
                <?php echo "<th class='title'><a href=".url('/watch/?v='.$videoid[0]).">".$recently_result[0]->title."</a></th>"; ?>
                <th class='result'>
                  <div class="hidden_box1">
                    <label for="label1">結果↓</label>
                    <input type="checkbox" id="label1"/>
                    <div class="hidden_show1">
                      <!--非表示ここから-->     
                      <?php echo $recently_result_image[0] ?>
                      <!--ここまで-->
                    </div>
                  </div>
                </th>
              </tr> 
              <tr class='recently_junken_table_cell'>
                <?php
                  echo "<th class='thumbnail'><a href=".url('/watch/?v='.$videoid[1])."><img class='thumbnail-img' src='https://img.youtube.com/vi/".$videoid[1]."/mqdefault.jpg' alt='".$recently_result[0]->title."'></a></th>";
                ?>
                <th class='upload_date'>{{$recently_result[1]->upload_date}}</th>
                <?php echo "<th class='title'><a href=".url('/watch/?v='.$videoid[1]).">".$recently_result[1]->title."</a></th>"; ?>
                <th class='result'>
                  <div class="hidden_box2">
                    <label for="label2">結果↓</label>
                    <input type="checkbox" id="label2"/>
                    <div class="hidden_show2">
                      <!--非表示ここから-->     
                      <?php echo $recently_result_image[1] ?>
                      <!--ここまで-->
                    </div>
                  </div>
                </th3
              </tr>
              <tr class='recently_junken_table_cell'>
                <?php
                  echo "<th class='thumbnail'><a href=".url('/watch/?v='.$videoid[2])."><img class='thumbnail-img' src='https://img.youtube.com/vi/".$videoid[2]."/mqdefault.jpg' alt='".$recently_result[0]->title."'></a></th>";
                  ?>
                <th class='upload_date'>{{$recently_result[2]->upload_date}}</th>
                <?php echo "<th class='title'><a href=".url('/watch/?v='.$videoid[2]).">".$recently_result[2]->title."</a></th>"; ?>
                <th class='result'>
                  <div class="hidden_box3">
                    <label for="label3">結果↓</label>
                    <input type="checkbox" id="label3"/>
                    <div class="hidden_show3">
                      <!--非表示ここから-->     
                      <?php echo $recently_result_image[2] ?>
                      <!--ここまで-->
                    </div>
                  </div>
                </th>
              </tr>
              <tr class='recently_junken_table_cell'>
                <?php
                  echo "<th class='thumbnail'><a href=".url('/watch/?v='.$videoid[3])."><img class='thumbnail-img' src='https://img.youtube.com/vi/".$videoid[3]."/mqdefault.jpg' alt='".$recently_result[0]->title."'></a></th>";
                  ?>
                <th class='upload_date'>{{$recently_result[3]->upload_date}}</th>
                <?php echo "<th class='title'><a href=".url('/watch/?v='.$videoid[3]).">".$recently_result[3]->title."</a></th>"; ?>
                <th class='result'>
                  <div class="hidden_box4">
                    <label for="label4">結果↓</label>
                    <input type="checkbox" id="label4"/>
                    <div class="hidden_show4">
                      <!--非表示ここから-->     
                      <?php echo $recently_result_image[3] ?>
                      <!--ここまで-->
                    </div>
                  </div>
                </th>
              </tr>
              <tr class='recently_junken_table_cell'>
                <?php
                  echo "<th class='thumbnail'><a href=".url('/watch/?v='.$videoid[4])."><img class='thumbnail-img' src='https://img.youtube.com/vi/".$videoid[4]."/mqdefault.jpg' alt='".$recently_result[0]->title."'></a></th>";
                  ?>
                <th class='upload_date'>{{$recently_result[4]->upload_date}}</th>
                <?php echo "<th class='title'><a href=".url('/watch/?v='.$videoid[4]).">".$recently_result[4]->title."</a></th>"; ?>
                <th class='result'>
                  <div class="hidden_box5">
                    <label for="label5">結果↓</label>
                    <input type="checkbox" id="label5"/>
                    <div class="hidden_show5">
                      <!--非表示ここから-->     
                      <?php echo $recently_result_image[4] ?>
                      <!--ここまで-->
                    </div>
                  </div>
                </th>
              </tr> 
            </tbody>
          </table>
        </div>
      </div>
    </div>


    <!-- ヒカキンブンブンじゃんけん天気予報 -->
    <div class="box18">
      <a name="weather_junken"></a>
      <h2 class="main-title">ブンブンじゃんけん天気予報</h2>
      <div class="box27">
      <span class="box-title">本日の天気予報</span>
      <?php
        echo "本日<b class='weather-update-date'>" . $latest_weather_date . "</b>に投稿されるHikakinTVチャンネルの動画には<br>";
        echo $latest_weather_result_image . "<br>";
        echo "を出すでしょう..."
      ?>
      <span class="weather-update-note">※毎朝6時更新</span>
      </div>
      <div class="dtable">
        <div class="dtable_c">
          <table class='all_junken_table' border='1'>
            <tbody>
              <tr>
                <th colspan='2' class='all_junken_exp'>{{$date_latest_junken[0]}}までの<br>予報数</th>
              </tr>
              <tr class='all_junken_table_cell'>
                <th colspan='2' class='all'>{{$weather_summary['total']}}</th>
              </tr>
              <tr>
                <th>予報通り</th>
                <th>予報と違った</th>
              </tr>
              <tr class='all_junken_table_cell'>
                <th class='each'>{{$weather_summary['win']}}</th>
                <th class='each'>{{$weather_summary['lose']}}</th>
              </tr>
            </tbody>
          </table>
        </div>

        <div class="dtable_c">
          <table class='recently_junken_table' border='1'>
            <tbody>
              <tr>
                <th colspan='3' class='recently_junken_exp'>最新の予報結果</th>
              </tr>
              <tr>
                <th>天気予報の手</th>
                <th>日付・結果</th>
                <th>ヒカキンの手</th>
              </tr>

              <tr class='recently_junken_table_cell'>
                <th class='result'>
                  <div class="hidden_box1">
                    <label for="label1_weather">予報↓</label>
                    <input type="checkbox" id="label1_weather"/>
                    <div class="hidden_show1">
                      <!--非表示ここから-->     
                      <?php echo $recently_weather_result_image[0] ?>
                      <!--ここまで-->
                    </div>
                  </div>
                </th>
                <th class='each' valign='top'>
                  <?php 
                    echo "<span class='weather-date'>" . $date_latest_junken[0] . "</span><br>";
                    if($weather_junken_match[0] == 1) {
                      echo "<span class='weather-win'>予報通り！</span>";
                    } else {
                      echo "<span class='weather-lose'>予報はずれ...</span>";
                    }
                  ?>
                </th>
                <th class='result'>
                  <div class="hidden_box1">
                    <label for="label1_hikakin">結果↓</label>
                    <input type="checkbox" id="label1_hikakin"/>
                    <div class="hidden_show1">
                      <!--非表示ここから-->     
                      <?php echo $recently_hikakin_result_image[0] ?>
                      <!--ここまで-->
                    </div>
                  </div>
                </th>
              </tr> 

              <tr class='recently_junken_table_cell'>
                <th class='result'>
                  <div class="hidden_box2">
                    <label for="label2_weather">予報↓</label>
                    <input type="checkbox" id="label2_weather"/>
                    <div class="hidden_show2">
                      <!--非表示ここから-->     
                      <?php echo $recently_weather_result_image[1] ?>
                      <!--ここまで-->
                    </div>
                  </div>
                </th>
                <th class='each' valign='top'>
                  <?php 
                    echo "<span class='weather-date'>" . $date_latest_junken[1] . "</span><br>";
                    if($weather_junken_match[1] == 1) {
                      echo "<span class='weather-win'>予報通り！</span>";
                    } else {
                      echo "<span class='weather-lose'>予報はずれ...</span>";
                    }
                  ?>
                </th>
                <th class='result'>
                  <div class="hidden_box2">
                    <label for="label2_hikakin">結果↓</label>
                    <input type="checkbox" id="label2_hikakin"/>
                    <div class="hidden_show2">
                      <!--非表示ここから-->     
                      <?php echo $recently_hikakin_result_image[1] ?>
                      <!--ここまで-->
                    </div>
                  </div>
                </th>
              </tr> 

              <tr class='recently_junken_table_cell'>
                <th class='result'>
                  <div class="hidden_box3">
                    <label for="label3_weather">予報↓</label>
                    <input type="checkbox" id="label3_weather"/>
                    <div class="hidden_show3">
                      <!--非表示ここから-->     
                      <?php echo $recently_weather_result_image[2] ?>
                      <!--ここまで-->
                    </div>
                  </div>
                </th>
                <th class='each' valign='top'>
                  <?php 
                    echo "<span class='weather-date'>" . $date_latest_junken[2] . "</span><br>";
                    if($weather_junken_match[2] == 1) {
                      echo "<span class='weather-win'>予報通り！</span>";
                    } else {
                      echo "<span class='weather-lose'>予報はずれ...</span>";
                    }
                  ?>
                </th>
                <th class='result'>
                  <div class="hidden_box3">
                    <label for="label3_hikakin">結果↓</label>
                    <input type="checkbox" id="label3_hikakin"/>
                    <div class="hidden_show3">
                      <!--非表示ここから-->     
                      <?php echo $recently_hikakin_result_image[2] ?>
                      <!--ここまで-->
                    </div>
                  </div>
                </th>
              </tr> 

              <tr class='recently_junken_table_cell'>
                <th class='result'>
                  <div class="hidden_box4">
                    <label for="label4_weather">予報↓</label>
                    <input type="checkbox" id="label4_weather"/>
                    <div class="hidden_show4">
                      <!--非表示ここから-->     
                      <?php echo $recently_weather_result_image[3] ?>
                      <!--ここまで-->
                    </div>
                  </div>
                </th>
                <th class='each' valign='top'>
                  <?php 
                    echo "<span class='weather-date'>" . $date_latest_junken[3] . "</span><br>";
                    if($weather_junken_match[3] == 1) {
                      echo "<span class='weather-win'>予報通り！</span>";
                    } else {
                      echo "<span class='weather-lose'>予報はずれ...</span>";
                    }
                  ?>
                </th>
                <th class='result'>
                  <div class="hidden_box4">
                    <label for="label4_hikakin">結果↓</label>
                    <input type="checkbox" id="label4_hikakin"/>
                    <div class="hidden_show4">
                      <!--非表示ここから-->     
                      <?php echo $recently_hikakin_result_image[3] ?>
                      <!--ここまで-->
                    </div>
                  </div>
                </th>
              </tr>

              <tr class='recently_junken_table_cell'>
                <th class='result'>
                  <div class="hidden_box5">
                    <label for="label5_weather">予報↓</label>
                    <input type="checkbox" id="label5_weather"/>
                    <div class="hidden_show5">
                      <!--非表示ここから-->     
                      <?php echo $recently_weather_result_image[4] ?>
                      <!--ここまで-->
                    </div>
                  </div>
                </th>
                <th class='each' valign='top'>
                  <?php 
                    echo "<span class='weather-date'>" . $date_latest_junken[4] . "</span><br>";
                    if($weather_junken_match[4] == 1) {
                      echo "<span class='weather-win'>予報通り！</span>";
                    } else {
                      echo "<span class='weather-lose'>予報はずれ...</span>";
                    }
                  ?>
                </th>
                <th class='result'>
                  <div class="hidden_box5">
                    <label for="label5_hikakin">結果↓</label>
                    <input type="checkbox" id="label5_hikakin"/>
                    <div class="hidden_show5">
                      <!--非表示ここから-->     
                      <?php echo $recently_hikakin_result_image[4] ?>
                      <!--ここまで-->
                    </div>
                  </div>
                </th>
              </tr>

            </tbody>
          </table>
        </div>
      </div>
    </div>



    <div class="box18">
      <h2 class="main-title">ブンブンじゃんけん検索</h2>

      <div class="double-search">
        <a name="keyword_search"></a>
        <div class="double-search_c">
        <h3 class="keyword-search-title">キーワード検索</h3>
          <div class="keyword-search-description">動画のタイトルとゲスト名の一部に一致する動画を検索します！</div>
          <!--キーワード検索フォーム-->
          <div class="keyword-form">
            <form method="get" action="{{ url('/keyword') }}">
              <div class="cp_iptxt">
                <div class="cp_label">キーワードを入力してね！</div>
                <input class="ef" type="text" name="keyword" placeholder="キーワード">
                <span class="focus_line"></span>
              </div>
              <div class="keyword-button">
                <input class="btn-square-so-pop" type="submit" value="検索">
              </div>
            </form>
          </div>
          <!--ここまで-->
        </div>

        <div class="double-search_c">
          <a name="guest_search"></a>
          <h3 class="guest-search-title">ゲスト検索</h3>
            <div class="guest-search-description">ブンブンじゃんけんに参加したゲストを検索します！</div>
            <!--ゲスト検索プルダウンメニュー-->
            <div class="guest-form">
              <div class="guest_label">ゲストを選んでね！</div>
                <select class="guest_iptxt" onChange="location.href=value;">
                  <option class="guest_ef" value="" placeholder="↓ ゲスト一覧"><span class="guest_name">↓ ゲスト一覧</span></option>
                  @foreach($all_guest as $d)
                  <?php
                    echo "<option class='guest_ef' value='".url('/guest/'.$d->guest)."'>".$d->guest."</option>";
                  ?>
                  @endforeach
                </select>
            </div>
            <!--ここまで-->
        </div>
      </div>

      <div class="double-main-search">
        <h4>🌟主な検索キーワード🌟</h4>
        <div class="keyword-list-one">
        <?php
          echo "<label for='label_guest'><a href='".url('/keyword/')."?keyword=マインクラフト'>マインクラフト</a></label>";
          echo "<label for='label_guest'><a href='".url('/keyword/')."?keyword=セイキン'>セイキン</a></label>";
          echo "<label for='label_guest'><a href='".url('/keyword/')."?keyword=セブンイレブン'>セブンイレブン</a></label>";
          echo "<label for='label_guest'><a href='".url('/keyword/')."?keyword=スイッチ'>スイッチ</a></label>";
          echo "<label for='label_guest'><a href='".url('/keyword/')."?keyword=スーパー'>スーパー</a></label>";
        ?>
        </div>
        <div class="keyword-list-two">
        <?php
          echo "<label for='label_guest'><a href='".url('/keyword/')."?keyword=uuum'>uuum</a></label>";
          echo "<label for='label_guest'><a href='".url('/keyword/')."?keyword=しまむら'>しまむら</a></label>";
          echo "<label for='label_guest'><a href='".url('/keyword/')."?keyword=泥ボーヒカキン'>泥ボーヒカキン</a></label>";
          echo "<label for='label_guest'><a href='".url('/keyword/')."?keyword=モンスト'>モンスト</a></label>";
          echo "<label for='label_guest'><a href='".url('/keyword/')."?keyword=マリオ'>マリオ</a></label>";
        ?>
        </div>
    </div>

    <h4>🌟主に出演したゲスト🌟</h4>
      <div class="guest-list-one">
      <?php
        for($i = 0; $i <= 4; $i++){
          echo "<label for='label_guest'><a href='".url('/guest/'.$most_guest[$i]->guest)."'>". $most_guest[$i]->guest ."</a></label>";
        }
      ?>
      </div>
      <div class="guest-list-two">
      <?php
        for($i = 5; $i <= 9; $i++){
          echo "<label for='label_guest'><a href='".url('/guest/'.$most_guest[$i]->guest)."'>". $most_guest[$i]->guest ."</a></label>";
        }
      ?>
      </div>
    </div>

    <div class="box18">
      <a name="statistics"></a>
      <h2 class="main-title">ブンブンじゃんけん統計データ</h2>
      <div class="double-statistics">
        <div class="double-statistics_right">
        <img class='statistics_junken' src='../img/statistics_junken.png' alt='statistics_junken'>
          <div class='statistics'>
            <label for='statistics'>
              <?php echo "<a href='".url('/statistics')."'>統計データ一覧</a>"; ?>
            </label>
          </div>
        </div>
        <div class="double-statistics_left">
        <img class='statistics_junken_pc' src='../img/statistics_junken_pc.png' alt='statistics_junken_pc'>
          <div class='statistics'>
            <label for='statistics'>
              <?php echo "<a href='".url('/statistics_download')."'>統計データダウンロード</a>"; ?>
            </label>
          </div>
        </div>
      </div>
    </div>

    <div class="box18">
      <h2 class="main-title">日時別ブンブンじゃんけん一覧</h2>
      <a name="date_list"></a>
      <?php
        $month = date('n');
      ?>

      <div class="double-date">
        <div class="double-date_c">
        <?php
        echo "<div class='year'>
                <label for='year'>
                  <a href='".url('/date/2022/')."'>2022年のブンブンじゃんけん</a>
                </label>
              </div>";
        ?>
          <h3 class="month_description">月別一覧</h3>
          <p></p>
          <div class="month-list-one">
        <?php
          if($month <= 6){
            for($i = 1; $i <= $month; $i++){
              echo "<label for='label_month'><a href='".url('/date/2022/'.$i)."'>". $i ."月</a></label>";
            }
          } else {
            for($i = 1; $i <= 6; $i++){
              echo "<label for='label_month'><a href='".url('/date/2022/'.$i)."'>". $i ."月</a></label>";
            }
          }
        ?>
        </div>
        <div class="month-list-two">
        <?php
          if($month >= 7){
            for($i = 7; $i <= $month; $i++){
              echo "<label for='label_month'><a href='".url('/date/2022/'.$i)."'>". $i ."月</a></label>";
            }
          }
        ?>
        </div>
        </div>
      </div>

      <div class="double-date">
        <div class="double-date_c">
        <?php
        echo "<div class='year'>
                <label for='year'>
                  <a href='".url('/date/2021/')."'>2021年のブンブンじゃんけん</a>
                </label>
              </div>";
        ?>
          <h3 class="month_description">月別一覧</h3>
          <p></p>
          <div class="month-list-one">
        <?php
          for($i = 1; $i <= 6; $i++){
            echo "<label for='label_month'><a href='".url('/date/2021/'.$i)."'>". $i ."月</a></label>";
          }
        ?>
        </div>
        <div class="month-list-two">
        <?php
          for($i = 7; $i <= 12; $i++){
            echo "<label for='label_month'><a href='".url('/date/2021/'.$i)."'>". $i ."月</a></label>";
          }
        ?>
        </div>
        </div>
        <div class="double-date_c">
        <?php
        echo "<div class='year'>
                <label for='year'>
                  <a href='".url('/date/2020/')."'>2020年のブンブンじゃんけん</a>
                </label>
              </div>";
        ?>
          <h3 class="month_description">月別一覧</h3>
          <p></p>
        <div class="month-list-one">
        <?php
          for($i = 1; $i <= 6; $i++){
            echo "<label for='label_month'><a href='".url('/date/2020/'.$i)."'>". $i ."月</a></label>";
          }
        ?>
        </div>
        <div class="month-list-two">
        <?php
          for($i = 7; $i <= 12; $i++){
            echo "<label for='label_month'><a href='".url('/date/2020/'.$i)."'>". $i ."月</a></label>";
          }
        ?>
        </div>
        </div>
      </div>

      <div class="double-date">
        <div class="double-date_c">
        <?php
        echo "<div class='year'>
                <label for='year'>
                  <a href='".url('/date/2019/')."'>2019年のブンブンじゃんけん</a>
                </label>
              </div>";
        ?>
          <h3 class="month_description">月別一覧</h3>
          <p></p>
        <div class="month-list-one">
        <?php
          for($i = 1; $i <= 6; $i++){
            echo "<label for='label_month'><a href='".url('/date/2019/'.$i)."'>". $i ."月</a></label>";
          }
        ?>
        </div>
        <div class="month-list-two">
        <?php
          for($i = 7; $i <= 12; $i++){
            echo "<label for='label_month'><a href='".url('/date/2019/'.$i)."'>". $i ."月</a></label>";
          }
        ?>
        </div>
        </div>

        <div class="double-date_c">
        <?php
        echo "<div class='year'>
                <label for='year'>
                  <a href='".url('/date/2018/')."'>2018年のブンブンじゃんけん</a>
                </label>
              </div>";
        ?>
          <h3 class="month_description">月別一覧</h3>
          <p></p>
        <div class="month-list-one">
        <?php
          for($i = 1; $i <= 6; $i++){
            echo "<label for='label_month'><a href='".url('/date/2018/'.$i)."'>". $i ."月</a></label>";
          }
        ?>
        </div>
        <div class="month-list-two">
        <?php
          for($i = 7; $i <= 12; $i++){
            echo "<label for='label_month'><a href='".url('/date/2018/'.$i)."'>". $i ."月</a></label>";
          }
        ?>
        </div>
        </div>
      </div>

      <div class="double-date">
        <div class="double-date_c">
        <?php
        echo "<div class='year'>
                <label for='year'>
                  <a href='".url('/date/2017/')."'>2017年のブンブンじゃんけん</a>
                </label>
              </div>";
        ?>
          <h3 class="month_description">月別一覧</h3>
          <p></p>
        <div class="month-list-one">
        <?php
          for($i = 1; $i <= 6; $i++){
            echo "<label for='label_month'><a href='".url('/date/2017/'.$i)."'>". $i ."月</a></label>";
          }
        ?>
        </div>
        <div class="month-list-two">
        <?php
          for($i = 7; $i <= 12; $i++){
            echo "<label for='label_month'><a href='".url('/date/2017/'.$i)."'>". $i ."月</a></label>";
          }
        ?>
        </div>
        </div>

        <div class="double-date_c">
        <?php
        echo "<div class='year'>
                <label for='year'>
                  <a href='".url('/date/2016/')."'>2016年のブンブンじゃんけん</a>
                </label>
              </div>";
        ?>
          <h3 class="month_description">月別一覧</h3>
          <p></p>
        <div class="month-list-one">
        <?php
          for($i = 1; $i <= 6; $i++){
            echo "<label for='label_month'><a href='".url('/date/2016/'.$i)."'>". $i ."月</a></label>";
          }
        ?>
        </div>
        <div class="month-list-two">
        <?php
          for($i = 7; $i <= 12; $i++){
            echo "<label for='label_month'><a href='".url('/date/2016/'.$i)."'>". $i ."月</a></label>";
          }
        ?>
        </div>
        </div>
      </div>


      <div class="double-date">
        <div class="double-date_c">
          <?php
          echo "<div class='year'>
                  <label for='year'>
                    <a href='".url('/date/2015/')."'>2015年のブンブンじゃんけん</a>
                  </label>
                </div>";
          ?>
            <h3 class="month_description">月別一覧</h3>
            <p></p>
          <div class="month-list-one">
          <?php
            for($i = 1; $i <= 6; $i++){
              echo "<label for='label_month'><a href='".url('/date/2015/'.$i)."'>". $i ."月</a></label>";
            }
          ?>
          </div>
          <div class="month-list-two">
          <?php
            for($i = 7; $i <= 12; $i++){
              echo "<label for='label_month'><a href='".url('/date/2015/'.$i)."'>". $i ."月</a></label>";
            }
          ?>
          </div>
        </div>

        <div class="double-date_c">
        <?php
        echo "<div class='year'>
                <label for='year'>
                  <a href='".url('/date/2014/')."'>2014年のブンブンじゃんけん</a>
                </label>
              </div>";
        ?>
          <h3 class="month_description">月別一覧</h3>
          <p></p>
        <div class="month-list-one">
        <?php
          for($i = 11; $i <= 12; $i++){
            echo "<label for='label_month'><a href='".url('/date/2014/'.$i)."'>". $i ."月</a></label>";
          }
        ?>
        </div>
        </div>
      </div>
    </div>
    
    <div class="box18">
      <h2 class="main-title">ブンブンじゃんけんの時系列推移</h2>
      <a name="transition"></a>
      <div class="hidden_transition">
        <label for="label_transition">クリックして表示↓</label>
        <input type="checkbox" id="label_transition"/>
          <div class="hidden_transition">
            <!--非表示ここから-->     
            <div class="flourish-embed flourish-bar-chart-race" data-src="visualisation/4910221">
              <script src="https://public.flourish.studio/resources/embed.js">
              </script>
            </div>
            <!--ここまで-->
          </div>
          動画link：<a href="https://www.youtube.com/watch?v=2DXEBxFh_ms">ヒカキンのブンブンじゃんけん　グー・チョキ・パー回数の推移【HIKAKIN】</a> 
      </div>
    </div>
  </div>


  <div class="Advertisement">
    <p class='youtube-botton'><label for='label_youtube' class='youtube'><a href='https://www.youtube.com/channel/UCf6Q3zGUw6ih1V_tpMWlsFg' target=_blank>Youtubeチャンネル</a></label></p>
    <p class='twitter-botton'><img class='twitter-aicon' src='../img/twitter.png' alt='Twitter'><label for='label_twitter' class='twitter'><a href='https://twitter.com/hikakinjunken1' target=_blank>Twitter</a></label></p>
  </div>
</main>
@endsection