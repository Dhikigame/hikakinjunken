@extends('layouts.default')

@section('title', 'はじめに - ヒカキンブンブンじゃんけん記録室')

@section('content')

<ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
  <li itemprop="itemListElement" itemscope
      itemtype="https://schema.org/ListItem">
    <a itemprop="item" href="/">
        <span itemprop="name">トップ</span>
    </a>
    <meta itemprop="position" content="1" />
  </li>
>
  <li itemprop="itemListElement" itemscope
      itemtype="https://schema.org/ListItem">
    <a itemprop="item" href="/introduction">
        <span itemprop="name">はじめに</span>
    </a>
    <meta itemprop="position" content="2" />
  </li>
</ol>

<h2 class="introduction-item">このサイトは...</h2>
<div>
    Youtuberヒカキンさんのチャンネル(<span></span><a href="https://www.youtube.com/channel/UCZf__ehlCEBPop-_sldpBUQ" target=_blank>HikakinTV</a></span>)に投稿された動画をまとめたサイト...でありますが、<br>
    真の目的は動画の最後に行われる「<b>ブンブンじゃんけん</b>」をまとめたサイトになります！<br>
    ヒカキンさんが動画内で行ったじゃんけんを逐次更新していきます！<br>
    <a href="/history">更新履歴</a>
    <div class="warning">※注意点</div>
    当サイトはヒカキンさん所属事務所である<a href="https://www.uuum.co.jp/" target=_blank>UUUM株式会社</a>へ問い合わせ、サイト公開に許可をいただいております。<br>
    ただし、アフィリエイト等の収益化に関しては許可されていませんので、その旨を承知した上運営しております。<br>
    当サイトで公開しているデータはご自由にお使いいただけますが、サイトに負荷をかける行為や他の利用者の妨げになる行為等お控えください。<br>
    このサイトについてお問い合わせやバグ・改善案等の打診は<a href="/contact">お問い合わせページ</a>もしくは<a class="twitter" href="https://twitter.com/hikakinjunken1" target=_blank>Twitter</a>にてご連絡をお待ちしています。<br>
    当サイトで掲載している情報はあくまでも運営者個人で確認した限りでの情報です。情報の誤りや偏見も含まれていますのでご注意ください。
</div>

<h2 class="introduction-item">ブンブンじゃんけんとは？</h2>
「ブンブンじゃんけん」はヒカキンさん得意のビートボックスで行う以下の流れのじゃんけんのことです。<br>
ヒカキンさんの動画をよくご覧になられている方はお馴染みですね！<br>
意外なことにあのサザエさんを超える約1500回ものじゃんけんを2014年11月〜動画内で行っています。すごいですね〜<br>
<!-- <div class="bunbunjunken-order"> -->
<ol class="bunbunjunken-order">
<b>ブンブンじゃんけんの流れ</b>
  <li>じゃんけんタ〜イム</li>
  <li>いきますよ！</li>
  <li>最初はブンブンじゃんけんポ〜ン!(じゃんけんの手を出す)</li>
  <li>バーイ</li>
</ol>
当サイトはそんなヒカキンさんが行うじゃんけんを記録し続けております。<br>
ただし、以下は記録の対象となっておりません。
<ul class="no-bunbunjunken">
<b>ブンブンじゃんけんの記録として認めないもの一覧</b>
  <li>あっち向いてホイなど他のルールのじゃんけん</li>
  <li>ブンブンじゃんけんをしたが、ヒカキンさんが消去・非公開にした動画</li>
  <li><a href="https://www.youtube.com/user/HikakinGames" target=_blank>HikakinGames</a></span>で行われたじゃんけん(他のチャンネルの動画にヒカキンさんが出演し、ブンブンじゃんけんが行われていたら統計対象)</li>
  <li>動画や生放送以外の外部イベントで行われたじゃんけん(私自身がチェックしきれないこともあり)</li>
</ul>

<h2 class="introduction-item">当サイトの使い方</h2>

各ページごとに説明します。<br>
<ul>
  <li class="site-item"><a href="/">トップページ</a></li>
  大きく分けて、「じゃんけんの累計と最近のじゃんけん結果」「ブンブンじゃんけん天気予報」「ブンブンじゃんけん検索」「日付別じゃんけん結果」「ブンブンじゃんけんの時系列推移」となっています。<br>
  このページから各ページに移行することができます。
  <li class="site-item"><a href="/">じゃんけんの累計と最近のじゃんけん結果</a></li>
  トップページから確認できます。<br>
  今まで行われたじゃんけんの総数とグー・パー・チョキのそれぞれの累計が表示されます。<br>
  また、最近投稿された動画5選において、じゃんけんの結果も確認できます！<br>
  [↓結果]をクリックするとじゃんけんの結果が表示されます。(ネタバレになりますので、最初に動画を見てから確認することをオススメします。)
  <li class="site-item"><a href="/#weather_junken">ブンブンじゃんけん天気予報</a></li>
  運営者が制作したヒカキンさんの投稿した動画で行われるじゃんけんをランダムで予測するプログラムを運用しております。<br>
  毎日朝6時に何の手を出すか予報していきます！<br>
  当たらなくてもクレームしないでください...<br>
  <li class="site-item"><a href="/#keyword_search">ブンブンじゃんけん検索</a></li>
  主に「好きなキーワードを入力して検索する方法」と「ヒカキンさんと一緒にじゃんけんしたゲスト」から検索する2種類あります。<br>
  また、「🌟みんながよく検索するキーワード🌟」と「🌟主に出演したゲスト🌟」のようなショートカットの項目も用意されております。
    <ul>
        <li><a href="/#keyword_search">キーワード検索</a></li>
        動画のタイトル、じゃんけんしたゲスト、備考に含まれる一致するキーワードを検索します。
        <li><a href="/#guest_search">ゲスト検索</a></li>
        ヒカキンさんと一緒にじゃんけんしたゲストをメニューから選択することで検索できます。
    </ul>
  <li class="site-item"><a href="/#statistics">ブンブンじゃんけん統計データ</a></li>
  今までのブンブンじゃんけんの手の一覧を確認できたり、統計データをダウンロードすることができます。<br>
　<li class="site-item"><a href="/#date_list">日時別ブンブンじゃんけん一覧</a></li>
  年代・月ごとに行われたブンブンじゃんけんの一覧を検索できます。<br>
  <li class="site-item"><a href="/#transition">ブンブンじゃんけんの時系列推移</a></li>
  今まで行われたブンブンじゃんけんのグー・パー・チョキの数の推移をグラフによって確認することができます。<br>
  グラフを見るには、[クリックして表示↓]をクリックすることで確認できます。<br>
  グラフ観覧中に左下のボタンをクリックすることで一時停止や初めから再生など行えます。<br>

  <li class="site-item">検索ページの見方</li>
  <img class='search_result' src='../img/search_result.png'><br>
  ①：ジャンケンの累計回数<br>
  ②：Youtube上のサムネイル<br>
  ③：動画を投稿した日付<br>
  ④：動画のタイトル<br>
  ⑤：動画内のブンブンじゃんけんの結果<br>
  ⑥：備考(じゃんけんに参加したゲストやどういう状況でじゃんけんしたか　など気になったことをまとめ)
  <ul>
    <b>主な書き方</b>
    <li>○○○○と：じゃんけんに参加したゲスト</li>
    <li>○○○○で：どういう状況でじゃんけんしたか</li>
  </ul>

  <li class="site-item">動画視聴ページの見方</li>
  動画のサムネイルまたはタイトルをクリックすると以下のような動画視聴ページに移行します。<br>
  <img class='video_view' src='../img/video_view.png'><br>
  ①：動画のタイトル<br>
  ②：動画の投稿日付<br>
  ③：動画再生ビューア(解像度は640✖︎360で固定)<br>
  ④：視聴している動画の前後にアップロードされた動画のリンク<br>
  ⑤：タグ一覧・登録機能<br>
    <ul>
    視聴している動画についているタグを確認・登録できます。(当サイト独自の機能)<br>
    タグを登録することで、<a href="/#keyword_search">キーワード検索</a>から登録したタグに含むワードを入力することで検索結果に動画を表示させることができます。<br><br>
    「タグを登録！」のフォームから最大5個までタグを登録することができます。(6個目を追加しても登録できません。)<br>
    タグを消去したい場合、❌をクリックすることで消去します。<br>
    </ul>
  ⑥：動画内のブンブンじゃんけんの結果<br>
    <ul>
    ネタバレ防止のため[結果↓]をクリックすることでじゃんけん結果を表示します。<br>
    </ul>
  ⑦：備考(じゃんけんに参加したゲストやどういう状況でじゃんけんしたか　など気になったことをまとめ)<br>
  ⑧：じゃんけんに参加したゲスト<br>
    <ul>
    ゲストのボタンをクリックすると、そのゲストが今までにじゃんけんに参加した動画を検索できます。
    </ul>
</ul>

<h2 class="introduction-item">サイトの管理者について</h2>
<div class="twitter-account">
    当サイトの管理人(Dhiki)はIT・統計・MAD動画関係で活動しています！<br>
    <a class="twitter" href="https://twitter.com/DHIKI_pico" target=_blank><img class='twitter' src='../img/twitter.png'>　管理人(Dhiki)のTwitter</a><br>
</div>
<div class="twitter-account">
    定期的に以下のアカウントでブンブンじゃんけんの統計について発信しております！<br>
    <a class="twitter" href="https://twitter.com/hikakinjunken1" target=_blank><img class='twitter' src='../img/twitter.png'>　ブンブンじゃんけんのBOTアカウント</a><br>
</div>


@endsection