@extends($search_layout)

@section('title')
  {{$search_keyword}} (キーワード検索) - ヒカキンブンブンじゃんけん記録室
@endsection

@section('content')

<ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
  <li itemprop="itemListElement" itemscope
      itemtype="https://schema.org/ListItem">
    <a itemprop="item" href="/">
        <span itemprop="name">トップ</span>
    </a>
    <meta itemprop="position" content="1" />
  </li>
>
  <li itemprop="itemListElement" itemscope
      itemtype="https://schema.org/ListItem">
    <a itemprop="item" href="/keyword?keyword=<?php echo $search_keyword ?> ">
        <span itemprop="name">[ <?php echo $search_keyword ?> ]でキーワード検索</span>
    </a>
    <meta itemprop="position" content="2" />
  </li>
</ol>

<h2 class="search-title">キーワード検索ページ</h2>

    <!--キーワード検索フォーム-->
    <div class="keyword-form-keyword-page">
      <form method="get" action="{{ url('/keyword') }}">
        <div class="cp_iptxt">
          <div class="cp_label">キーワードを入力してね！</div>
          <input class="ef" type="text" name="keyword" value="<?php echo $search_keyword ?>" placeholder="キーワード">
          <span class="focus_line"></span>
        </div>

        <div>
          <input id="asc" type="radio" name="date" value="asc" <?php if($date == "asc"){ echo "checked";} ?> ><label for="asc">日付が古い順</label>
          <input id="desc" type="radio" name="date" value="desc" <?php if($date == "desc"){ echo "checked";} ?> ><label for="desc">日付が新しい順</label>
        </div>
        <div>
        <input id="all" type="radio" name="junken" value="all" <?php if($junken == "all"){ echo "checked";} ?> ><label for="all">全ての手</label>
          <input id="goo" type="radio" name="junken" value="グー" <?php if($junken == "グー"){ echo "checked";} ?> ><label for="goo">グー</label>
          <input id="choki" type="radio" name="junken" value="チョキ" <?php if($junken == "チョキ"){ echo "checked";} ?> ><label for="choki">チョキ</label>
          <input id="par" type="radio" name="junken" value="パー" <?php if($junken == "パー"){ echo "checked";} ?> ><label for="par">パー</label>
          <input id="rest" type="radio" name="junken" value="休み" <?php if($junken == "休み"){ echo "checked";} ?> ><label for="rest">休み</label>
        </div>

        <div class="keyword-button">
          <input class="keyword-btn-square-so-pop" type="submit" value="検索">
        </div>
      </form>
    </div>

  <div class="keyword-search">
    <h4>🌟みんながよく検索するキーワード🌟</h4>
    <div class="keyword-list-one">
    <?php
      echo "<label for='label_guest'><a href='".url('/keyword/')."?keyword=マインクラフト'>マインクラフト</a></label>";
      echo "<label for='label_guest'><a href='".url('/keyword/')."?keyword=セイキン'>セイキン</a></label>";
      echo "<label for='label_guest'><a href='".url('/keyword/')."?keyword=セブンイレブン'>セブンイレブン</a></label>";
      echo "<label for='label_guest'><a href='".url('/keyword/')."?keyword=スイッチ'>スイッチ</a></label>";
      echo "<label for='label_guest'><a href='".url('/keyword/')."?keyword=スーパー'>スーパー</a></label>";
    ?>
    </div>
    <div class="keyword-list-two">
    <?php
      echo "<label for='label_guest'><a href='".url('/keyword/')."?keyword=uuum'>uuum</a></label>";
      echo "<label for='label_guest'><a href='".url('/keyword/')."?keyword=しまむら'>しまむら</a></label>";
      echo "<label for='label_guest'><a href='".url('/keyword/')."?keyword=泥ボーヒカキン'>泥ボーヒカキン</a></label>";
      echo "<label for='label_guest'><a href='".url('/keyword/')."?keyword=モンスト'>モンスト</a></label>";
      echo "<label for='label_guest'><a href='".url('/keyword/')."?keyword=マリオ'>マリオ</a></label>";
    ?>
    </div>
  </div>

<h3 class="search-count">
  「{{$search_keyword}}」を含む動画 全<?php echo $search_count ?>件
<?php
  if($search_count != 0){
    echo "<span class='page-num'>(".$data_submin."〜".$data_submax."件)</span>";
  }
    $search_conditions = "(";
    if($date == "desc") { $search_conditions .= "日付が新しい順、";}
    else { $search_conditions .= "日付が古い順、";}
  
    if($junken == "all") { $search_conditions .= "ジャンケンの手：全ての手";}
    else if($junken == "グー")  { $search_conditions .= "ジャンケンの手：グー";}
    else if($junken == "チョキ")  { $search_conditions .= "ジャンケンの手：チョキ";}
    else if($junken == "パー")  { $search_conditions .= "ジャンケンの手：パー";}
    else if($junken == "休み")  { $search_conditions .= "ジャンケンが休み";}
    else { $search_conditions .= "ジャンケンの手：全ての手";}
  
    $search_conditions .= ")";
    echo $search_conditions;
?>
</h3>

@endsection