<!-- footer -->
  <footer>
　　<ul class="footer-menu">
     <li><a href="/introduction">はじめに</a> ｜</li>
     <li><a href="/#keyword_search">じゃんけん検索</a> |</li>
     <li><a href="/#date_list">じゃんけん一覧</a> ｜</li>
     <li><a href="/contact">お問い合わせ</a></li>
    </ul>
    <p>Copyright © 2022 ヒカキンじゃんけん記録室 (<a class="twitter" href="https://twitter.com/hikakinjunken1" target=_blank>@hikakinjunken1</a>) All rights reserved.</p>
  </footer>
<!-- /footer -->