<!-- header -->
<div class="dropdown">
    <div class="menu">
      <a href="/introduction"><div class="menu-title">はじめに</div></a>
    </div>

    <div class="menu">
      <div class="menu-title">じゃんけん検索 ▼</div>
        <div class="sub-menu">
          <ul>
            <a href="/#keyword_search"><li>キーワード検索</li></a>
            <a href="/#guest_search"><li>ゲスト検索</li></a>
          </ul>
        </div>
    </div>
    <div class="menu">
      <div class="menu-title">じゃんけん一覧 ▼</div>
        <div class="sub-menu">
          <ul>
            <?php
            echo '<a href="'.url('/date/2022/').'"><li>2022年のジャンケン結果</li></a>';
            echo '<a href="'.url('/date/2021/').'"><li>2021年のジャンケン結果</li></a>';
            echo '<a href="'.url('/date/2020/').'"><li>2020年のジャンケン結果</li></a>';
            echo '<a href="'.url('/date/2019/').'"><li>2019年のジャンケン結果</li></a>';
            echo '<a href="'.url('/date/2018/').'"><li>2018年のジャンケン結果</li></a>';
            echo '<a href="'.url('/date/2017/').'"><li>2017年のジャンケン結果</li></a>';
            echo '<a href="'.url('/date/2016/').'"><li>2016年のジャンケン結果</li></a>';
            echo '<a href="'.url('/date/2015/').'"><li>2015年のジャンケン結果</li></a>';
            echo '<a href="'.url('/date/2014/').'"><li>2014年のジャンケン結果</li></a>';
            ?>
          </ul>
        </div>
    </div>
    <div class="menu">
      <a href="/contact"><div class="menu-title">お問い合わせ</div></a>
    </div>
</div>
<!-- /header -->