@extends('layouts.footer')

<!DOCTYPE html>
<html lang="ja">
<head>
  <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
  <link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
  <link rel="manifest" href="/manifest.json">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
  <meta name="theme-color" content="#ffffff">

  <meta name="description" content="Youtuberヒカキンさんのブンブンじゃんけんを記録・予想するサイトです！">

  <meta charset="UTF-8">
  <meta http-equiv="Pragma" content="no-cache">
  <meta http-equiv="Cache-Control" content="no-cache">
  <meta http-equiv="Expires" content="0">
  <title>@yield('title')</title>
  <link href="{{ asset('/css/styles.css') }}" rel="stylesheet">
  <link href="{{ asset('/css/navigation.css') }}" rel="stylesheet">
  <link href="{{ asset('/css/search.css') }}" rel="stylesheet">
  <link href="{{ asset('/css/pagination.css') }}" rel="stylesheet">
  <script src="{{ asset('/js/header.js') }}"></script>

  @if(env('APP_ENV') == 'production')
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-85902327-5"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-85902327-5');
    </script>
  @endif
</head>
<body>
<?php
  echo "<a href='/'><img class='top-img' src='../../img/top.png'></a>";
?>

<!-- header -->
<div class="dropdown">
    <div class="menu">
      <a href="/introduction"><div class="menu-title">はじめに</div></a>
    </div>

    <div class="menu">
      <div class="menu-title">じゃんけん検索 ▼</div>
        <div class="sub-menu">
          <ul>
            <a href="/#keyword_search"><li>キーワード検索</li></a>
            <a href="/#guest_search"><li>ゲスト検索</li></a>
          </ul>
        </div>
    </div>
    <div class="menu">
      <div class="menu-title">じゃんけん一覧 ▼</div>
        <div class="sub-menu">
          <ul>
            <?php
            echo '<a href="'.url('/date/2022/').'"><li>2022年のジャンケン結果</li></a>';
            echo '<a href="'.url('/date/2021/').'"><li>2021年のジャンケン結果</li></a>';
            echo '<a href="'.url('/date/2020/').'"><li>2020年のジャンケン結果</li></a>';
            echo '<a href="'.url('/date/2019/').'"><li>2019年のジャンケン結果</li></a>';
            echo '<a href="'.url('/date/2018/').'"><li>2018年のジャンケン結果</li></a>';
            echo '<a href="'.url('/date/2017/').'"><li>2017年のジャンケン結果</li></a>';
            echo '<a href="'.url('/date/2016/').'"><li>2016年のジャンケン結果</li></a>';
            echo '<a href="'.url('/date/2015/').'"><li>2015年のジャンケン結果</li></a>';
            echo '<a href="'.url('/date/2014/').'"><li>2014年のジャンケン結果</li></a>';
            ?>
          </ul>
        </div>
    </div>
    <div class="menu">
      <a href="/contact"><div class="menu-title">お問い合わせ</div></a>
    </div>
</div>
<!-- /header -->

    @yield('content')
@php
  // 1ページに表示する動画の件数をカウントするカウンタ
  $page_count = 0;
  // 10件ごとに回数〜備考欄を表示させるカウンタ
  $loop_count = 0;
@endphp
      <table class='date_junken_table' border='1'>
        <tbody>
          <tr class='date_junken_exp'>
            <th>回数</th>
            <th>サムネイル</th>
            <th>投稿日時</th>
            <th>動画タイトル</th>
            <th>結果</th>
            <th>備考</th>
          </tr>
        @foreach($data_offset as $d)
          <tr class='date_junken_table_cell'>
            <?php
            // Youtubeの動画IDを切り出し
            $videoid = substr($d->videoid, -11);

            if($d->result != "休み"){
              echo "<th class='junken_count'>$d->junken_count</th>";
            } else {
              echo "<th class='rest_count'>$d->result</th>";
            }
            ?>
            <?php
              echo "<th class='search-thumbnail'><a href=".url('/watch/?v='.$videoid)."><img class='search-thumbnail-img' src='https://img.youtube.com/vi/".$videoid."/mqdefault.jpg'></a></th>";
            ?>
            <th class='upload_date'>{{$d->upload_date}}</th>
            <?php echo "<th class='title'><a href=".url('/watch/?v='.$videoid).">$d->title</a></th>"; ?>
            <?php
            if($d->result == "グー"){
              echo "<th class='goo-result'><img class='goo' src='../../img/goo.png'><br>".$d->result."</th>";
            } 

            if($d->result == "パー"){
              echo "<th class='par-result'><img class='par' src='../../img/par.png'><br>".$d->result."</th>";
            } 

            if($d->result == "チョキ"){
              echo "<th class='choki-result'><img class='choki' src='../../img/choki.png'><br>".$d->result."</th>";
            }

            if($d->result == "休み"){
              echo "<th class='rest'>".$d->result."</th>";
            }
            ?>
            
            <th class='note'>
              <?php echo $d->note; ?>
            </th>
          </tr> 
          @php
            $page_count++;
            $loop_count++;
          @endphp

          @if ($page_count >= 30)
            @break
          @endif

          @if ($loop_count >= 10)
            <tr class='date_junken_exp'>
              <th>回数</th>
              <th>サムネイル</th>
              <th>投稿日時</th>
              <th>動画タイトル</th>
              <th>結果</th>
              <th>備考</th>
            </tr>
            @php
              $loop_count = 0;
            @endphp
          @endif
        @endforeach
        </tbody>
      </table>

  <div class="center">
    <ul class="pagination">
      <?php
      /* キーワード検索ページネーション */
      if($search_target == "keyword") {
        // <<,< prev
        if($page == 1) {
          echo "<li><a href='#'>&lt;&lt;</a></li>";
          echo "<li><a href='#'>&lt;</a></li>";
        } else {
          echo "<li><a href='".url('/keyword?keyword='.$search_keyword)."&page=1&date=".$date."&junken=".$junken."'>&lt;&lt;</a></li>";
          $prev_page = $page - 1;
          echo "<li><a href='".url('/keyword?keyword='.$search_keyword)."&page=".$prev_page."&date=".$date."&junken=".$junken."'>&lt;</a></li>";
        }

        for($i = 1; $i <= $pagination_num; $i++) {
          if($i == $page) {
            echo "<li><a class='active' href='#'>".$i."</a></li>";
          } else {
            echo "<li><a href='".url('/keyword?keyword='.$search_keyword)."&page=".$i."&date=".$date."&junken=".$junken."'>".$i."</a></li>";
          }
        }
        // >,>> next
        if($page == $pagination_num) {
          echo "<li><a href='#'>&gt;</a></li>";
          echo "<li><a href='#'>&gt;&gt;</a></li>";
        } else {
          $next_page = $page + 1;
          echo "<li><a href='".url('/keyword?keyword='.$search_keyword)."&page=".$next_page."&date=".$date."&junken=".$junken."'>&gt;</a></li>";
          echo "<li><a href='".url('/keyword?keyword='.$search_keyword)."&page=".$pagination_num."&date=".$date."&junken=".$junken."'>&gt;&gt;</a></li>";
        }
      }

      /* ゲスト検索ページネーション */
      if($search_target == "guest") {
        // <<,< prev
        if($page == 1) {
          echo "<li><a href='#'>&lt;&lt;</a></li>";
          echo "<li><a href='#'>&lt;</a></li>";
        } else {
          echo "<li><a href='".url('/guest/'.$search_guest)."?page=1&date=".$date."&junken=".$junken."'>&lt;&lt;</a></li>";
          $prev_page = $page - 1;
          echo "<li><a href='".url('/guest/'.$search_guest)."?page=".$prev_page."&date=".$date."&junken=".$junken."'>&lt;</a></li>";
        }

        for($i = 1; $i <= $pagination_num; $i++) {
          if($i == $page) {
            echo "<li><a class='active' href='#'>".$i."</a></li>";
          } else {
            echo "<li><a href='".url('/guest/'.$search_guest)."?page=".$i."&date=".$date."&junken=".$junken."'>".$i."</a></li>";
          }
        }
        // >,>> next
        if($page == $pagination_num) {
          echo "<li><a href='#'>&gt;</a></li>";
          echo "<li><a href='#'>&gt;&gt;</a></li>";
        } else {
          $next_page = $page + 1;
          echo "<li><a href='".url('/guest/'.$search_guest)."?page=".$next_page."&date=".$date."&junken=".$junken."'>&gt;</a></li>";
          echo "<li><a href='".url('/guest/'.$search_guest)."?page=".$pagination_num."&date=".$date."&junken=".$junken."'>&gt;&gt;</a></li>";
        }
      }

      /* 日付検索ページネーション */
        // 日付検索ページネーション(年・月)
        if($search_target == "date" && $month != null) {
          // <<,< prev
          if($page == 1) {
            echo "<li><a href='#'>&lt;&lt;</a></li>";
            echo "<li><a href='#'>&lt;</a></li>";
          } else {
            echo "<li><a href='".url('/date/'.$year.'/'.$month)."?page=1&date=".$date."&junken=".$junken."'>&lt;&lt;</a></li>";
            $prev_page = $page - 1;
            echo "<li><a href='".url('/date/'.$year.'/'.$month)."?page=".$prev_page."&date=".$date."&junken=".$junken."'>&lt;</a></li>";
          }

          for($i = 1; $i <= $pagination_num; $i++) {
            if($i == $page) {
              echo "<li><a class='active' href='#'>".$i."</a></li>";
            } else {
              echo "<li><a href='".url('/date/'.$year.'/'.$month)."?page=".$i."&date=".$date."&junken=".$junken."'>".$i."</a></li>";
            }
          }
          // >,>> next
          if($page == $pagination_num) {
            echo "<li><a href='#'>&gt;</a></li>";
            echo "<li><a href='#'>&gt;&gt;</a></li>";
          } else {
            $next_page = $page + 1;
            echo "<li><a href='".url('/date/'.$year.'/'.$month)."?page=".$next_page."&date=".$date."&junken=".$junken."'>&gt;</a></li>";
            echo "<li><a href='".url('/date/'.$year.'/'.$month)."?page=".$pagination_num."&date=".$date."&junken=".$junken."'>&gt;&gt;</a></li>";
          }
        }
        // 日付検索ページネーション(年のみ)
        if($search_target == "date" && $month == null) {
          // <<,< prev
          if($page == 1) {
            echo "<li><a href='#'>&lt;&lt;</a></li>";
            echo "<li><a href='#'>&lt;</a></li>";
          } else {
            echo "<li><a href='".url('/date/'.$year)."?page=1&date=".$date."&junken=".$junken."'>&lt;&lt;</a></li>";
            $prev_page = $page - 1;
            echo "<li><a href='".url('/date/'.$year)."?page=".$prev_page."&date=".$date."&junken=".$junken."'>&lt;</a></li>";
          }

          for($i = 1; $i <= $pagination_num; $i++) {
            if($i == $page) {
              echo "<li><a class='active' href='#'>".$i."</a></li>";
            } else {
              echo "<li><a href='".url('/date/'.$year)."?page=".$i."&date=".$date."&junken=".$junken."'>".$i."</a></li>";
            }
          }
          // >,>> next
          if($page == $pagination_num) {
            echo "<li><a href='#'>&gt;</a></li>";
            echo "<li><a href='#'>&gt;&gt;</a></li>";
          } else {
            $next_page = $page + 1;
            echo "<li><a href='".url('/date/'.$year)."?page=".$next_page."&date=".$date."&junken=".$junken."'>&gt;</a></li>";
            echo "<li><a href='".url('/date/'.$year)."?page=".$pagination_num."&date=".$date."&junken=".$junken."'>&gt;&gt;</a></li>";
          }
        }
      ?>
    </ul>
  </div>

  @yield('footer')
</body>
</html>