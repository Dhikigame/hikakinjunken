@extends('layouts.footer')

<!DOCTYPE html>
<html lang="ja">
<head>
  <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
  <link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
  <link rel="manifest" href="/manifest.json">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
  <meta name="theme-color" content="#ffffff">

  <meta name="description" content="Youtuberヒカキンさんのブンブンじゃんけんを記録・予想するサイトです！">

  <meta charset="UTF-8">
  <title>@yield('title')</title>
  <link href="{{ asset('/css/styles.css') }}" rel="stylesheet">
  <link href="{{ asset('/css/search.css') }}" rel="stylesheet">
  <script src="{{ asset('/js/header.js') }}"></script>

  @if(env('APP_ENV') == 'production')
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-85902327-5"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-85902327-5');
    </script>
  @endif
</head>
<body>
<?php
  echo "<a href='/'><img class='top-img' src='../../img/top.png'></a>";
?>
<!-- header -->
<div class="dropdown">
    <div class="menu">
      <a href="/introduction"><div class="menu-title">はじめに</div></a>
    </div>

    <div class="menu">
      <div class="menu-title">じゃんけん検索 ▼</div>
        <div class="sub-menu">
          <ul>
            <a href="/#keyword_search"><li>キーワード検索</li></a>
            <a href="/#guest_search"><li>ゲスト検索</li></a>
          </ul>
        </div>
    </div>
    <div class="menu">
      <div class="menu-title">じゃんけん一覧 ▼</div>
        <div class="sub-menu">
          <ul>
            <?php
            echo '<a href="'.url('/date/2022/').'"><li>2022年のジャンケン結果</li></a>';
            echo '<a href="'.url('/date/2021/').'"><li>2021年のジャンケン結果</li></a>';
            echo '<a href="'.url('/date/2020/').'"><li>2020年のジャンケン結果</li></a>';
            echo '<a href="'.url('/date/2019/').'"><li>2019年のジャンケン結果</li></a>';
            echo '<a href="'.url('/date/2018/').'"><li>2018年のジャンケン結果</li></a>';
            echo '<a href="'.url('/date/2017/').'"><li>2017年のジャンケン結果</li></a>';
            echo '<a href="'.url('/date/2016/').'"><li>2016年のジャンケン結果</li></a>';
            echo '<a href="'.url('/date/2015/').'"><li>2015年のジャンケン結果</li></a>';
            echo '<a href="'.url('/date/2014/').'"><li>2014年のジャンケン結果</li></a>';
            ?>
          </ul>
        </div>
    </div>
    <div class="menu">
      <a href="/contact"><div class="menu-title">お問い合わせ</div></a>
    </div>
</div>
<!-- /header -->

    @yield('content')
<?php
  // キーワード検索エラー
  if($search_error === "no keyword"){
    echo "<h2>キーワードが入力されていません！</h2>";
  }
  if($search_error === "no result"){
    echo "<h2>相当する動画が見つかりません！</h2>";
  }
  // ゲスト検索エラー
  if($search_error === "no guest"){
    echo "<h2>選択したゲストのジャンケン結果が存在しません！</h2>";
  }
  // ゲスト検索エラー
  if($search_error === "no date"){
    echo "<h2>検索した日付のジャンケン結果が存在しません！</h2>";
  }
?>

  @yield('footer')
</body>
</html>