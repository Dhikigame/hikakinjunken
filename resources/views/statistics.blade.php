@extends('layouts.default')

@section('title', '統計データ一覧 - ヒカキンブンブンじゃんけん記録室')

@section('content')

<ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
  <li itemprop="itemListElement" itemscope
      itemtype="https://schema.org/ListItem">
    <a itemprop="item" href="/">
        <span itemprop="name">トップ</span>
    </a>
    <meta itemprop="position" content="1" />
  </li>
>
  <li itemprop="itemListElement" itemscope
      itemtype="https://schema.org/ListItem">
    <a itemprop="item" href="/statistics">
        <span itemprop="name">統計データ</span>
    </a>
    <meta itemprop="position" content="2" />
  </li>
</ol>

<h2 class="introduction-item">年別ごとのブンブンじゃんけん統計</h2>

      <div id='moveup' class="dtable">
        <div class="dtable_c">
          <table class='all_junken_table' border='1'>
            <tbody>
              <tr>
                <th class='all_junken_exp'>年</th>
                <th class='all_junken_exp'>動画数</th>
                <th class='all_junken_exp'>じゃんけん数</th>
                <th class='all_junken_exp'>グー</th>
                <th class='all_junken_exp'>パー</th>
                <th class='all_junken_exp'>チョキ</th>
              </tr>
            <?php
            for($year = 2014; $year <= $now_year; $year++) {
              echo "<tr class='all_junken_table_cell'>";
                echo "<th class='all'>" . $year . "年</th>";
                echo "<th class='all'>" . count($year_junken[$year]['all']) . "</th>";
                echo "<th class='all'>" . count($year_junken[$year]['junken']) . "</th>";
                echo "<th class='all'>" . count($year_junken[$year]['goo']) . "</th>";
                echo "<th class='all'>" . count($year_junken[$year]['par']) . "</th>";
                echo "<th class='all'>" . count($year_junken[$year]['choki']) . "</th>";
              echo "</tr>";
            }
            ?>
            </tbody>
          </table>
        </div>
      </div>


<h2 class="introduction-item">月別ごとのブンブンじゃんけん統計</h2>

<?php
      for($year = 2014; $year <= $now_year; $year++) {
        echo "<span class='year-list-one'><a href='#" . $year . "'>" . $year . "年</a></span>";
      }
?>

      <div class="dtable">
        <div class="dtable_c">
          <table class='all_junken_table' border='1'>
            <tbody>
              <tr>
                <th class='all_junken_exp'>年</th>
                <th class='all_junken_exp'>月</th>
                <th class='all_junken_exp'>動画数</th>
                <th class='all_junken_exp'>じゃんけん数</th>
                <th class='all_junken_exp'>グー</th>
                <th class='all_junken_exp'>パー</th>
                <th class='all_junken_exp'>チョキ</th>
              </tr>
            <?php
            for($year = 2014; $year <= $now_year; $year++) {
              for($month = 1; $month <= 12; $month++) {
                echo "<tr class='all_junken_table_cell'>";
                if($month == 1) {
                  echo "<th rowspan='12' class='all'>".$year."年</th>";
                }
                  echo "<th id='". $year . "' class='all'>".$month."月</th>";
                  echo "<th class='all'>" . count($month_junken[$year][$month]['all']) . "</th>";
                  echo "<th class='all'>" . count($month_junken[$year][$month]['junken']) . "</th>";
                  echo "<th class='all'>" . count($month_junken[$year][$month]['goo']) . "</th>";
                  echo "<th class='all'>" . count($month_junken[$year][$month]['par']) . "</th>";
                  echo "<th class='all'>" . count($month_junken[$year][$month]['choki']) . "</th>";
                echo "</tr>";
              }
              if($year != $now_year) {
                echo "<tr>";
                  echo "<th class='all_junken_exp'>年</th>";
                  echo "<th class='all_junken_exp'>月</th>";
                  echo "<th class='all_junken_exp'>動画数</th>";
                  echo "<th class='all_junken_exp'>じゃんけん数</th>";
                  echo "<th class='all_junken_exp'>グー</th>";
                  echo "<th class='all_junken_exp'>パー</th>";
                  echo "<th class='all_junken_exp'>チョキ</th>";
                echo "</tr>";
              }
            }
            ?>
            </tbody>
          </table>
        </div>
      </div>

      <?php
        echo "<span class='moveup'><a href='#moveup'>上に戻る</a></span>";
      ?>
@endsection