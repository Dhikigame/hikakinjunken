@extends('layouts.default')

@section('title', '統計データダウンロード - ヒカキンブンブンじゃんけん記録室')

@section('content')

<ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
  <li itemprop="itemListElement" itemscope
      itemtype="https://schema.org/ListItem">
    <a itemprop="item" href="/">
        <span itemprop="name">トップ</span>
    </a>
    <meta itemprop="position" content="1" />
  </li>
>
  <li itemprop="itemListElement" itemscope
      itemtype="https://schema.org/ListItem">
    <a itemprop="item" href="/statistics">
        <span itemprop="name">統計データダウンロード</span>
    </a>
    <meta itemprop="position" content="2" />
  </li>
</ol>

<h2 class="introduction-item">このページは...?</h2>
今までの統計データをCSV形式でダウンロードできます。(毎朝7時更新)<br>
よろしければ、このデータをダウンロードして解析や研究に使用していただいても構いません。<br>
ダウンロードできる情報は以下の通りです。
<ul>
  <li>動画のタイトル：Youtubeにアップロードされた動画のタイトル</li>
  <li>動画のURL：Youtubeにアップロードされた動画のURL</li>
  <li>動画の投稿日時：Youtubeにアップロードされた動画の投稿日時</li>
  <li>ジャンケンの結果：動画で行われたブンブンじゃんけんで出した手の結果(その動画でじゃんけんしていなければ「休み」と表記)</li>
  <li>ゲスト01〜20：動画で行われたブンブンじゃんけんで一緒にじゃんけんしたゲストの名前</li>
</ul>
※当サイトで掲載している情報はあくまでも運営者個人で確認した限りでの情報です。情報の誤りが含まれている可能性もありますのでご注意ください。<br>
※基本的にHikakinTVチャンネルで投稿された動画が統計対象ですが、一部HikakinTV以外でじゃんけんした動画も含まれています。<br>


<h2 class="introduction-item">全期間の統計データダウンロード</h2>

    <div id='moveup' class="all-statistics-download">
      <div class="all-statistics-download">
        <div class="all-statistics-download_c">
          <div class="statistics-form">
          <h3 class="statistics-search-title">全期間の統計データ</h3>
            <div class="statistics-search-description">2014年〜の全期間の統計データをダウンロードします。</div>
            <div class='statistics'>
              <label for='statistics'>
                <a href="./csv/全期間のブンブンじゃんけん統計.csv">CSVダウンロード</a>
              </label>
            </div>
          </div>
        </div>
      </div>
    </div>


<h2 class="introduction-item">年別の統計データダウンロード</h2>

      <div class="year-statistics-download">
<?php
      $now_year = date('Y');
      for($year = 2014; $year <= $now_year; $year++) {
        echo <<<EOT
        <div class="year-statistics-download">
          <div class="year-statistics-download_c">
            <div class="statistics-form">
        EOT;
        echo '<h3 class="statistics-search-title">' . $year . '年の統計データ</h3>';
        echo '<div class="statistics-search-description">' . $year . '年にブンブンじゃんけんした統計データをダウンロードします。</div>';
                  echo <<<EOT
                  <div class="statistics">
                    <label for="statistics">
                  EOT;
                echo '<a href="./csv/' . $year . '年のブンブンじゃんけん統計.csv">CSVダウンロード</a>';
            echo <<<EOT
                    </label>
                  </div>
            </div>
          </div>
        </div>
        EOT;
      }
?>
      </div>
            
              
              

<h2 class="introduction-item">月別の統計データダウンロード</h2>

      <div class="month-statistics-download">
<?php
      for($month = 1; $month <= 12; $month++) {
        echo <<<EOT
        <div class="month-statistics-download">
          <div class="month-statistics-download_c">
            <div class="statistics-form">
        EOT;
        echo '<h3 class="statistics-search-title">' . $month . '月の統計データ</h3>';
          echo '<div class="statistics-search-description">' . $month . '月にブンブンじゃんけんした統計データをダウンロードします。</div>';
                  echo <<<EOT
                  <div class="statistics">
                    <label for="statistics">
                  EOT;
                echo '<a href="./csv/' . $month . '月のブンブンじゃんけん統計.csv">CSVダウンロード</a>';
            echo <<<EOT
                    </label>
                  </div>
                </div>
          </div>
        </div>
        EOT;
      }
?>
      </div>

<h2 class="introduction-item">ゲスト別の統計データダウンロード</h2>

      <div class="guest-statistics-download">
        <div class="guest-statistics-download">
          <div class="guest-statistics-download_c">
            <div class="statistics-form">
            <h3 class="statistics-search-title">ゲスト参加の統計データ</h3>
            <div class="statistics-search-description">ゲストと一緒にじゃんけんしている動画の統計データをダウンロードします。</div>
              <div class='statistics'>
                <label for='statistics'>
                  <a href="./csv/ゲスト参加のブンブンじゃんけん統計.csv">CSVダウンロード</a>
                </label>
              </div>
            </div>
          </div>
        </div>

        <div class="guest-statistics-download">
          <div class="guest-statistics-download_c">
            <div class="statistics-form">
            <h3 class="statistics-search-title">ゲストなしの統計データ</h3>
            <div class="statistics-search-description">ヒカキンさんが一人でじゃんけんしている動画の統計データをダウンロードします。</div>
              <div class='statistics'>
                <label for='statistics'>
                  <a href="./csv/ゲストなしのブンブンじゃんけん統計.csv">CSVダウンロード</a>
                </label>
              </div>
            </div>
          </div>
        </div>
      </div>
      <?php
        echo "<div><span class='moveup'><a href='#moveup'>上に戻る</a></span></div>";
      ?>
@endsection