@extends($watch_layout)

@section('title')
  {{$viewvideo["title"]}} (動画視聴ページ) - ヒカキンブンブンじゃんけん記録室
@endsection

@section('content')

<ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
  <li itemprop="itemListElement" itemscope
      itemtype="https://schema.org/ListItem">
    <a itemprop="item" href="/">
        <span itemprop="name">トップ</span>
    </a>
    <meta itemprop="position" content="1" />
  </li>
>
  <li itemprop="itemListElement" itemscope
      itemtype="https://schema.org/ListItem">
    <a itemprop="item" href="/watch?v=<?php echo $videoid ?> ">
        <span itemprop="name">[ <?php echo $viewvideo["title"] ?> ]の動画視聴ページ</span>
    </a>
    <meta itemprop="position" content="2" />
  </li>
</ol>

<h2 class="watch-title">動画視聴ページ</h2>

<div class="viewvideoinfo">
  <span class="viewvideotitle">
<?php
echo $viewvideo["title"];
?>
  </span>
  <br>
  <span class="viewvideodate">
<?php
echo $viewvideo["uploaddate"];
?>
  </span>
</div>

<?php
echo '<iframe id="ytplayer" type="text/html" width="640" height="360" src="https://www.youtube.com/embed/'.$videoid.'?autoplay=1&origin=https://hikakinjunken.tk" frameborder="0">';
echo '</iframe>';
?>

<div class="beforeaftervideo">
<?php
if($beforevideo["uploaddate"] != ""){
    echo "<span class='beforevideo'>";
    echo "<a href=".url('/watch/?v='.$beforevideo["videoid"]).">" . "< " . $beforevideo["uploaddate"] . "</a>";
    echo "</span>";
}
if($aftervideo["uploaddate"] != ""){
    echo "<span class='aftervideo'>";
    echo "<a href=".url('/watch/?v='.$aftervideo["videoid"]).">" . $aftervideo["uploaddate"] . " > </a>";
    echo "</span>";
}
// echo $view_counter;
// var_dump($beforeaftervideo);
// echo $junken;
?>
</div>

<!-- <div class="tags-junken"> -->
  <!-- <div class="tags-junken_c"> -->
<div class="dtable">
  <div class="dtable_c">
    <table class='watch_table' border='1'>
      <tbody>
        <tr>
          <th><p>タグ一覧</p></th>
          <th>
            ジャンケン結果
          </th>
          <th class="note">
            備考
          </th>
        </tr>
        <tr>
          <th>
            <div class="tags-list">
            <?php
              if($tags->tag1 != null) {
                echo "<label for='label_guest'><a href=".url('/keyword/')."?keyword=".$tags->tag1.">".$tags->tag1."</a></label>";
                echo "<a href=".url('/watch/')."?v=".$videoid."&delete=tag1>❌</a>";
              }
              if($tags->tag2 != null) {
                echo "<label for='label_guest'><a href=".url('/keyword/')."?keyword=".$tags->tag2.">".$tags->tag2."</a></label>";
                echo "<a href=".url('/watch/')."?v=".$videoid."&delete=tag2>❌</a>";
              }
              if($tags->tag3 != null) {
                echo "<label for='label_guest'><a href=".url('/keyword/')."?keyword=".$tags->tag3.">".$tags->tag3."</a></label>";
                echo "<a href=".url('/watch/')."?v=".$videoid."&delete=tag3>❌</a>";
              }
              if($tags->tag4 != null) {
                echo "<label for='label_guest'><a href=".url('/keyword/')."?keyword=".$tags->tag4.">".$tags->tag4."</a></label>";
                echo "<a href=".url('/watch/')."?v=".$videoid."&delete=tag4>❌</a>";
              }
              if($tags->tag5 != null) {
                echo "<label for='label_guest'><a href=".url('/keyword/')."?keyword=".$tags->tag5.">".$tags->tag5."</a></label>";
                echo "<a href=".url('/watch/')."?v=".$videoid."&delete=tag5>❌</a>";
              }
            ?>
              <!--タグ登録フォーム-->
              <div class="keyword-form">
                <form method="post" action="{{ url('/watch') }}">
                {{ csrf_field() }}
                <?php
                // echo '<form method="post" action='.url('/watch?v='.$videoid).'>';
                ?>
                  <div class="cp_iptxt">
                    <div class="cp_label">タグを登録！</div>
                    <?php
                    echo '<input type="hidden" name="v" value='.$videoid.'>';
                    ?>
                    <input type="hidden" name="register" value="register">
                    <input class="ef" type="text" name="tag" placeholder="タグ">
                      <span class="focus_line"></span>
                  </div>
                  @if ($errors->has('body'))
                  <span class="error">{{ $errors->first('body') }}</span>
                  @endif
                  <div class="keyword-button">
                    <input class="btn-square-so-pop" type="submit" value="登録">
                  </div>
                </form>
              </div>
              <!--ここまで-->
            </div>
          </th>
          <th>
            <div class="hidden_box1">
              <label for="label1">結果↓</label>
              <input type="checkbox" id="label1"/>
              <div class="hidden_show1">
              <!--非表示ここから-->     
              <?php echo $junken ?>
              <!--ここまで-->
            </div>
          </div>
          </th>
          <th>
            <div>
              <?php
                echo $viewvideo["note"];
              ?>
            </div>
          </th>
        </tr>
        <tr border="3">
          <td colspan="3">
            一緒にジャンケンしたゲスト一覧
          </td>
        </tr>
        <tr border="3">
          <td colspan="3">
            <?php
            for($guest_count = 0;$guest_count <= 19; $guest_count++) {
              if($guest[$guest_count] !== "") {
                echo "<span class='guest-list'><label for='label_guest'><a href='".url('/guest/'.$guest[$guest_count])."'>" . $guest[$guest_count] . "</a></label></span>  ";
              } else if($guest[$guest_count] === "" && $guest_count > 0) {
                break;
              }
              if($guest[$guest_count] === "" && $guest_count == 0) {
                echo "<b>ゲスト不在</b>";
              }
            }
            ?>
          </td>
        </tr>
      </tbody>
    </table>
  </div>
</div>

@endsection