<div class="viewvideoinfo">
  <span class="viewvideotitle">
    コメント投稿
  </span>
  <!-- <form method="post" action="{{ action('CommentsController@store', $videoid) }}"> -->
  <form method="post" action="{{ action('CommentsController@store', $viewvideo) }}">
    {{ csrf_field() }}
    <div class="textarea-wrap">
      <textarea name="name" rows="4" cols="80" value="{{ old('body') }}"></textarea>
    </div>
    @if ($errors->has('body'))
    <span class="error">{{ $errors->first('body') }}</span>
    @endif
    <div class="comment-button">
      <input class="comment-btn-square-so-pop" type="submit" value="投稿">
    </div>
  </form>
</div>