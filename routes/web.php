<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// トップ
Route::get('/', 'HikakinJunkenController@index');
// はじめに
Route::get('/introduction', 'HikakinJunkenController@introduction');
// 更新履歴
Route::get('/history', 'HikakinJunkenController@history');
// 統計ページ
Route::get('/statistics', 'HikakinJunkenController@statistics');
// 統計ページダウンロード
Route::get('/statistics_download', 'HikakinJunkenController@statistics_download');
// キーワード検索
Route::get('/keyword/{keyword?}', 'HikakinJunkenController@keyword');
// ゲスト検索
Route::get('/guest/{guest}', 'HikakinJunkenController@guest');
// 日付検索
Route::get('/date/{year}/{month?}', 'HikakinJunkenController@date');
Route::get('/date/{year}', 'HikakinJunkenController@date');
// お問い合わせ
Route::get('/contact', 'ContactController@contact');
Route::post('/contact', 'ContactController@contact');

// 動画視聴ページ
Route::get('/watch', 'VideoViewController@watch');
Route::post('/watch', 'VideoViewController@watch');
Route::get('/watch/{tag?}', 'VideoViewController@tagstore');

// Route::get('hikakinjunken/model/{type?}', 'HikakinJunkenController@model');

// エラーページ
// Route::get('error/{code}', function ($code) {
//     abort($code);
//   });